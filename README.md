# OOP_Exams

This repository contains my personal solution of some exams of the Object Oriented Course at PoliTO. 

## Instruction

Download the entire repository. Then open Eclipse, right click > import > General > Existing Projects into Workspace. Then in the wizard, chose the project you want to import. In the folder, there is a .project file, that Eclipse use to identify his project. 

Thank you for downloading my code!
