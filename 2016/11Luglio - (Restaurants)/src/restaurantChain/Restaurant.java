package restaurantChain;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Restaurant {
	private String name;
	private int tables;
	private int reservedTable = 0;
	private int refused = 0;

	private HashMap<String, Menu> menus = new HashMap<>();
	private HashMap<String, Group> groups = new HashMap<>();
	
	/**
	 * @param name
	 * @param tables
	 */
	public Restaurant(String name, int tables) {
		this.name = name;
		this.tables = tables;
	}

	public String getName(){
		return name;
	}
	
	public void addMenu(String name, double price) throws InvalidName{
		if(menus.containsKey(name))
			throw new InvalidName();
		menus.put(name, new Menu(name, price));
	}
	
	public int reserve(String name, int persons) throws InvalidName{
		if(groups.containsKey(name))
			throw new InvalidName();
		Group g = new Group(name, persons);
		if((tables - reservedTable) > persons/4) {
			reservedTable += persons/4;
			if(persons%4 != 0)
				reservedTable++;
			groups.put(name, g);
			return persons%4 == 0 ? persons/4:persons/4+1;
		}
		refused += persons;
		return 0;
	}
	
	public int getRefused(){
		return refused;
	}
	
	public int getUnusedTables(){
		return tables - reservedTable;
	}
	
	public boolean order(String name, String... menu) throws InvalidName{
		if(!groups.containsKey(name))
			throw new InvalidName();
		if(menu.length < groups.get(name).getnPeople())
			return false;
		Group g = groups.get(name);
		for(String menuName:menu) {
			if(!menus.containsKey(menuName))
				throw new InvalidName();
			Menu m = menus.get(menuName);
			g.addMenu(m);
		}
		g.ordered = true;
		return true;
	}
	
	public List<String> getUnordered(){
		return groups.values().stream()
				.filter(g -> g.isUnordered())
				.map(Group::getReferenceName)
				.sorted()
				.collect(Collectors.toList());
				
	}
	
	public double pay(String name) throws InvalidName{
		if(!groups.containsKey(name))
			throw new InvalidName();
		Group g = groups.get(name);
		if(g.isUnordered())
			return 0.0;
		g.pay();
		return g.getMenus().stream()
				.mapToDouble(Menu::getPrice).sum();
	}
	
	public List<String> getUnpaid(){
		return groups.values().stream()
				.filter(g -> g.isOrdered())
				.filter(g ->!g.hasPayed())
				.map(Group::getReferenceName)
				.sorted()
				.collect(Collectors.toList());
	}
	
	public double getIncome(){
		return groups.values().stream()
				.filter(g ->g.hasPayed())
				.flatMap(g ->g.getMenus().stream())
				.mapToDouble(Menu::getPrice)
				.sum();
	}

}
