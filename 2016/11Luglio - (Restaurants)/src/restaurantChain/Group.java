package restaurantChain;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Group {
	private String referenceName;
	private int nPeople;
	private List<Menu> menus = new LinkedList<>();
	boolean ordered = false;
	private boolean payed = false;
	/**
	 * @param referenceName
	 * @param nPeople
	 */
	public Group(String referenceName, int nPeople) {
		this.referenceName = referenceName;
		this.nPeople = nPeople;
	}
	
	/**
	 * @return the referenceName
	 */
	public String getReferenceName() {
		return referenceName;
	}
	
	/**
	 * @return the nPeople
	 */
	public int getnPeople() {
		return nPeople;
	}

	public void addMenu(Menu m) {
		menus.add(m);
	}

	/**
	 * @return the menus
	 */
	public List<Menu> getMenus() {
		return menus;
	}

	public boolean isOrdered() {
		return ordered;		
	}

	public boolean isUnordered() {
		return !ordered;
	}

	public void pay() {
		payed = true;
	}
	
	public boolean hasPayed() {
		return payed;
	}

}
