package restaurantChain;

import java.util.HashMap;
import java.util.List;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

public class Chain {	
	private HashMap<String, Restaurant> restaurants = new HashMap<>();

		public void addRestaurant(String name, int tables) throws InvalidName{
			if(restaurants.containsKey(name))
				throw new InvalidName();
			restaurants.put(name, new Restaurant(name, tables));
		}
		
		public Restaurant getRestaurant(String name) throws InvalidName{
			if(restaurants.containsKey(name))
				return restaurants.get(name);
			throw new InvalidName();
		}
		
		public List<Restaurant> sortByIncome(){
			return restaurants.values().stream()
					.sorted(comparing(Restaurant::getIncome, reverseOrder()))
					.collect(toList());
		}
		
		public List<Restaurant> sortByRefused(){
			return restaurants.values().stream()
					.sorted(comparing(Restaurant::getRefused))
					.collect(toList());
		}
		
		public List<Restaurant> sortByUnusedTables(){
			return restaurants.values().stream()
					.sorted(comparing(Restaurant::getUnusedTables))
					.collect(toList());
		}
}
