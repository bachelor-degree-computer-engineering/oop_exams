package restaurantChain;

public class Menu {
	private String name;
	private double price;
	
	/**
	 * @param name
	 * @param price
	 */
	public Menu(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	
	
}
