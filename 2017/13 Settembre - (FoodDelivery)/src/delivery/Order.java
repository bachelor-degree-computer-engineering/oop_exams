package delivery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import delivery.Delivery.OrderStatus;

public class Order {
	private int orderId, customerId;
	private Map<Menu, Integer> items = new HashMap<>();
	private OrderStatus status;
	
	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public Order(int orderId, int customerId) {
		this.orderId = orderId;
		this.customerId = customerId;
		status = Delivery.OrderStatus.NEW;
	}
	
	
	
	public int getOrderId() {
		return orderId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public Map<Menu, Integer> getItems() {
		return items;
	}

	public int addItem(Menu menu, int qty) {
		int orderQty = 0;
		if(items.containsKey(menu)) {
			orderQty = items.get(menu);
		}
		items.put(menu, orderQty + qty);
		return orderQty + qty;
	}
	
	public double totalPrice() {
		return items.entrySet().stream().mapToDouble(e -> e.getKey().getPrice()*e.getValue()).sum();
	}

	public List<String> toListString() {
		return items.entrySet().stream().
				
				map(e -> e.getKey().getDescription() + " " + e.getValue()).
				collect(Collectors.toList());
		}

	public int totalTime() {
		return 5 + 15 + items.entrySet().stream().mapToInt(e -> e.getKey().getPrepTime()).max().getAsInt();
	}
}
