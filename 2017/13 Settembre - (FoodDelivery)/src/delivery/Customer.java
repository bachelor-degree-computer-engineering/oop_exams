package delivery;

import java.util.LinkedList;
import java.util.List;

public class Customer {
	private int id;
	private String name, address, phoneNo, email;
	List<Order> orders = new LinkedList<>();
	
	public Customer(int id, String name, String address, String phoneNo, String email) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.phoneNo = phoneNo;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public String getEmail() {
		return email;
	}
	
	@Override
	public String toString() {
		return name + ", " + address + ", " + phoneNo + ", " + email; 
	}

	public List<Order> getOrders() {
		return orders;
	}
	
	
}
