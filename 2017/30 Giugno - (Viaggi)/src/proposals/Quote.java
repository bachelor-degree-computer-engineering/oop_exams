package proposals;

public class Quote {
	private String operatorName;
	private int amount;
	private Proposal p;
	int nChoice = 0;
	
	/**
	 * @param operatorName
	 * @param amount
	 */
	public Quote(Proposal p,String operatorName, int amount) {
		this.p = p;
		this.operatorName = operatorName;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}
	
	public String getProposalName() {
	       	return p.getName();
	}
	
	public String getDestination() {
		return p.getDestination();
	}
	public String getOperatorName() {
	       	return operatorName;
	}
	
	public int getNChoices() {
	       	return nChoice;
	}
	

}
