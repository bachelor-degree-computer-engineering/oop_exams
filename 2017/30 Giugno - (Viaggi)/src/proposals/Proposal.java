package proposals;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;

public class Proposal {
	private String name;
	private String destination;
	private SortedSet<String> assocUser = new TreeSet<>();
	private Set<String> allUser;
	private  Map<String, Operator> allOperators;
	private SortedSet<String> assocOperators = new TreeSet<>();
	private Map<String, Quote> quotes = new HashMap<>();
	
	/**
	 * @param name
	 * @param destination
	 */
	public Proposal(Set<String> allUser, Map<String, Operator> allOperators, String name, String destination) {
		this.allUser = allUser;
		this.allOperators = allOperators;
		this.name = name;
		this.destination = destination;
	}

	public String getDestination(){
		return destination;
	}

	public List<String> setUsers(String... userNames) {
		List<String> refused = new ArrayList<>();
		for(String username:userNames) {
			if(allUser.contains(username))
				assocUser.add(username);
			else {
				refused.add(username);
			}
		}
		Collections.sort(refused);
		return refused;
	}


	public SortedSet<String> getUsers() {
		return assocUser;
	}
	
	public List<String> setOperators(String... operatorNames) {
		List<String> refused = new ArrayList<>();
		for(String operator:operatorNames) {
			if(allOperators.containsKey(operator) && allOperators.get(operator).dest.contains(this.destination))
				assocOperators.add(operator);
			else
				refused.add(operator);
			
		}
		Collections.sort(refused);
		return refused;
		
	}
	
	public SortedSet<String> getOperators() {
		return assocOperators;
	}
	
	public void addQuote(String operatorName, int amount) throws ProposalException {
		if(!assocOperators.contains(operatorName))
			throw new ProposalException();
		Quote q = new Quote(this, operatorName, amount);
		quotes.put(operatorName, q);
	}

	public List<Quote> getQuotes() {
		return quotes.values().stream()
				.sorted(comparing(Quote::getAmount, reverseOrder())
						.thenComparing(Quote::getOperatorName)
						)
				.collect(toList());
	}
	
	//R4
		public void makeChoice (String userName, String operatorName) throws ProposalException {
			if(!assocUser.contains(userName) || !quotes.keySet().contains(operatorName))
					throw new ProposalException();
			quotes.get(operatorName).nChoice++;
		}
		
		public Quote getWinningQuote () {
			return quotes.values().stream()
					.collect(
						maxBy(
							comparing(Quote::getNChoices)
							.thenComparing(Quote::getAmount, reverseOrder())
						)
					).orElse(null);
		}


		public String getName() {
			return name;
		}	
}
