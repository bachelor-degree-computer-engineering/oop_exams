package proposals;

import java.util.Set;
import java.util.TreeSet;

public class Operator {
	private String name;
    Set<String> dest = new TreeSet<>();
	/**
	 * @param name
	 */
	public Operator(String name) {
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
    
	public boolean equals(Operator o) {
		return o.getName().equals(name);
	}
}
