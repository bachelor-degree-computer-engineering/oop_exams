package proposals;
import java.util.*;


import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;

public class ProposalHandling {
	private TreeSet<String> users = new TreeSet<>();
	private TreeMap<String, Operator> operators = new TreeMap<>();
	private TreeSet<String> destinations = new TreeSet<>();
	private TreeMap<String, Proposal> proposals = new TreeMap<>();

    //R1
	public int addUsers(String... userNames) {
		for(String username:userNames) {
			users.add(username);
		}
		return userNames.length;
	}
	
	public void addOperator(String operatorName, String... destinationNames) throws ProposalException {			
		if(operators.containsKey(operatorName))
			throw new ProposalException();
		Operator o = new Operator(operatorName);
		for(String dests:destinationNames) {
			if(destinations.contains(dests));
			destinations.add(dests);
			o.dest.add(dests);
		}
		operators.put(operatorName, o);
	}

	public List<String> getDestOperators(String destinationName) {
		return operators.values().stream()
				.filter(o -> o.dest.contains(destinationName))
				.map(Operator::getName)
				.collect(toList());
	}
	
//R2
	public Proposal newProposal(String name, String destinationName) throws ProposalException {
		if(proposals.containsKey(name))
			throw new ProposalException();
		if(!destinations.contains(destinationName))
				throw new ProposalException();
		Proposal p = new Proposal(users, operators, name, destinationName);
		proposals.put(name, p);
		return p;
	}
	
//R3

	public List<Quote> getUserQuotes (String userName) {
        	return null;
	}
	
//R5
	public SortedMap<String, Integer> totalAmountOfQuotesPerDestination() {
		return proposals.values().stream()
				.flatMap(p -> p.getQuotes().stream())
				.collect(groupingBy(
							Quote::getDestination,
							TreeMap::new,
							summingInt(Quote::getAmount)
						)
					);
	}
	
	@SuppressWarnings("unchecked")
	public SortedMap<Integer, List<String>> operatorsPerNumberOfQuotes() {
        	return proposals.values().stream()
        			.flatMap(p ->p.getQuotes().stream())
        			.collect(groupingBy(
        					Quote::getOperatorName,
        					TreeMap::new,
        					counting()
        				)	
        			).entrySet().stream()
        			.collect(groupingBy(
        					(Map.Entry<String, Long> e) -> e.getValue(),
        					() -> new TreeMap(reverseOrder()),
        					mapping(Map.Entry<String, Long>::getKey, toList())
        				)
        			);
	}

	public SortedMap<String, Long> numberOfUsersPerDestination() {
		return proposals.values().stream()
				.filter(p ->p.getUsers().size()>0)
				.collect(groupingBy(
						Proposal::getDestination,
						TreeMap::new,
						counting()
					)
				);
	}
	
	@SuppressWarnings("unchecked")
	public SortedMap<Integer, List<String>> proposalsPerNumberOfQuotes() {
        	return proposals.values().stream()
        			.flatMap(p -> p .getQuotes().stream())
        			.collect(groupingBy(
        					Quote::getProposalName,
        					TreeMap::new,
        					counting()
        				)
        			).entrySet().stream()
        			.collect(groupingBy(
        					(Map.Entry<String, Long> e) -> e.getValue(),
        					() -> new TreeMap(reverseOrder()),
        					mapping(Map.Entry<String, Long>::getKey, toList())
        				)
        			);
	}
}
