package groups;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Supplier {
	private String name;
	private Set<String> products = new TreeSet<>();
	private Set<Group> groups = new HashSet<>();
	
	int nBids = 0;
	
	
	/**
	 * @return the nBids
	 */
	public int getnBids() {
		return nBids;
	}

	public Supplier(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addProduct(String product) {
		products.add(product);
	}
	
	public List<String> getProducts(){
		return new ArrayList<String>(products);
	}

	public void addGroup(Group g) {
		groups.add(g);
	}
}
