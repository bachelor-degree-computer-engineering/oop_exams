package groups;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static java.util.Comparator.*;

public class Customer {
	private String name;
	private Set<Group> groups = new TreeSet<>(comparing(Group::getName));
	
	
	public Customer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void addGroup(Group g) {
		groups.add(g);
	}
	
	public List<String> getGroup(){
		return groups.stream().map(Group::getName).collect(Collectors.toList());

	}
}

