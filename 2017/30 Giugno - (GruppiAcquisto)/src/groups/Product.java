package groups;

import java.util.LinkedList;
import java.util.List;

public class Product {
	private String name;
	private List<String> suppliers = new LinkedList<>();
	
	
	public Product(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void addSupplier(String supplierName) {
		suppliers.add(supplierName);
	}
}
