package groups;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Group {
	private String name;
	private Product product;
	private Set<Customer> customers = new HashSet<>();
	private Set<Supplier> suppliers = new HashSet<>();
	private Set<Bid> bids = new HashSet<>();

	public Group(String name, Product product) {
		this.name = name;
		this.product = product;
	}

	public String getName() {
		return name;
	}

	public Product getProduct() {
		return product;
	}

	public Set<Customer> getCustomers() {
		return customers;
	}

	public void addCustomer(Customer c) {
		customers.add(c);
		
	}

	public void addSupplier(Supplier s) {
		suppliers.add(s);
	}

	public boolean hasSupplier(Supplier s) {
		return suppliers.stream().anyMatch(x -> x.getName().equals(s.getName()));
	}

	public void addBid(Bid b) {
		bids.add(b);
	}

	public Set<Bid> getBids() {
		return bids;
	}
	
	public Bid getBid(String name) {
		return bids.stream().filter(b ->b.getSupplier().getName().equals(name)).collect(Collectors.toList()).get(0);
	}

	public Integer getMaxPrice() {
		return bids.stream().max(Comparator.comparing(Bid::getPrice)).get().getPrice();
	}
	
}
