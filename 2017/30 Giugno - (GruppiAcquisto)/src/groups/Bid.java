package groups;

public class Bid {
	private Group group;
	private Supplier supplier;
	private int price;
	
	Integer nVotes = 0;
	public Bid(Group group, Supplier supplier, int price) {
		this.group = group;
		this.supplier = supplier;
		this.price = price;
	}

	public Group getGroup() {
		return group;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public Integer getPrice() {
		return price;
	}
	
	public String getProductName() {
		return group.getProduct().getName();
	}
	@Override
	public String toString() {
		return supplier.getName() + ":" + price;
	}
	
	public Integer getNVotes() {
		return nVotes;
	}
	
}
