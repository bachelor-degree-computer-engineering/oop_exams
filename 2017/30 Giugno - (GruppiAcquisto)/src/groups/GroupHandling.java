package groups;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;
public class GroupHandling {
	private Map<String, Product> products = new HashMap<>();
	private Map<String, Supplier> suppliers = new HashMap<>();
	private Map<String, Group> groups = new HashMap<>();
	private Map<String, Customer> customers = new HashMap<>();
//R1	
	public void addProduct (String productTypeName, String... supplierNames) 
			throws GroupHandlingException {
		if(products.containsKey(productTypeName))
			throw new GroupHandlingException("Duplicated product");
		Product p = new Product(productTypeName);
		products.put(productTypeName, p);
		for(String supplierName:supplierNames) {
			if(!suppliers.containsKey(supplierName)) {
				Supplier s = new Supplier(supplierName);
				suppliers.put(supplierName, s);
			}
			Supplier s = suppliers.get(supplierName);
			p.addSupplier(supplierName);
			s.addProduct(p.getName());
		}
	}
	
	public List<String> getProductTypes (String supplierName) {
		return suppliers.get(supplierName).getProducts();
	}
	
//R2	
	public void addGroup (String name, String productName, String... customerNames) 
			throws GroupHandlingException {
		if(groups.containsKey(name))
			throw new GroupHandlingException("Duplicated group");
		Product p = products.get(productName);
		if(p == null)
			throw new GroupHandlingException("Unckown product");
		Group g = new Group(name, p);
		groups.put(name, g);
		for(String customerName:customerNames) {
			Customer c = customers.get(customerName);
			if(c == null) {
				c = new Customer(customerName);
				customers.put(customerName, c);
			}
			g.addCustomer(c);
			c.addGroup(g);
		}
	}
	
	public List<String> getGroups (String customerName) {
		return customers.get(customerName).getGroup();
	}

//R3
	public void setSuppliers (String groupName, String... supplierNames)
			throws GroupHandlingException {
		Group g = groups.get(groupName);
		for(String supplierName:supplierNames) {
			Supplier s = suppliers.get(supplierName);
			if(s == null)
				throw new GroupHandlingException("Unknown supplier");
			if(!s.getProducts().contains(g.getProduct().getName()))
				throw new GroupHandlingException("Supplier does not support product");
			s.addGroup(g);
			g.addSupplier(s);
		}
	}
	
	public void addBid (String groupName, String supplierName, int price)
			throws GroupHandlingException {
		Group g = groups.get(groupName);
		Supplier s = suppliers.get(supplierName);
		if(!g.hasSupplier(s))
			throw new GroupHandlingException("Group not support supplier");
		Bid b = new Bid(g, s, price);
		s.nBids++;
		g.addBid(b);
	}
	
	public String getBids (String groupName) {
        return groups.get(groupName).getBids().stream().
        		sorted(comparing(Bid::getPrice).thenComparing(b -> b.getSupplier().getName())).
        		map(Bid::toString).
        		collect(joining(","));
	}
	
	
//R4	
	public void vote (String groupName, String customerName, String supplierName)
			throws GroupHandlingException {
		Group g = groups.get(groupName);
		if(!g.getCustomers().stream().anyMatch(c ->c.getName().equals(customerName)))
			throw new GroupHandlingException("Customer not in group.");
		if(!g.getBids().stream().anyMatch(b ->b.getSupplier().getName().equals(supplierName)))
			throw new GroupHandlingException("Supplier has not bids");
		g.getBid(supplierName).nVotes++;
		
	}
	
	public String  getVotes (String groupName) {
        return groups.get(groupName).getBids().stream().
        		filter(b ->b.nVotes>0).
        		sorted(comparing(b -> b.getSupplier().getName())).
        		map(b -> b.getSupplier().getName() + ":" + b.nVotes).
        		collect(joining(","));
	}
	
	public String getWinningBid (String groupName) {
        if(groups.get(groupName).getBids().size() == 0)
        	return null;
        Bid b = groups.get(groupName).getBids().stream().
        		max(comparing(Bid::getNVotes).thenComparing(Bid::getPrice, reverseOrder()))
        		.get();
        return b.getSupplier().getName() + ":" + b.nVotes;
	}
	
//R5
	public SortedMap<String, Integer> maxPricePerProductType() { //serve toMap
        return groups.values().stream().flatMap(g -> g.getBids().stream())
        		.collect(toMap(
        					Bid::getProductName, 
        					Bid::getPrice,
        					(p1, p2) -> {return p1 >= p2 ? p1 : p2;},
        					TreeMap::new
        				)
        				);
	}
	
	public SortedMap<Integer, List<String>> suppliersPerNumberOfBids() {
		return suppliers.values().stream()
				.filter(f -> f.nBids > 0)
				.collect(toMap(
						Supplier::getName,
						Supplier::getnBids
					)
				).entrySet().stream()
				.collect(groupingBy(
						Map.Entry::getValue,
						() -> new TreeMap<Integer, List<String>>(reverseOrder()),
						mapping(Map.Entry::getKey, toList())
					)
				);
				
	}
	
	public SortedMap<String, Long> numberOfCustomersPerProductType() {
        return groups.values().stream()
        		.filter(g -> g.getCustomers().size() > 0)
        		.collect(groupingBy(
        				(Group g) -> g.getProduct().getName(),
        				TreeMap::new, 
        				Collectors.summingLong(g -> g.getCustomers().size())
        				
        			)
        		);
	}
	
}
