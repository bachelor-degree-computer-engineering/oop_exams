package carShare;

public class Car {
	private Parking parking;
	private String licencePlate;
	private double minRate;
	private double kmRate;
	private boolean reserved = false;
	private boolean inUse = false;
	/**
	 * @param parking
	 * @param licencePlate
	 * @param minRate
	 * @param kmRate
	 */
	public Car(Parking parking, String licencePlate, double minRate, double kmRate) {
		this.parking = parking;
		this.licencePlate = licencePlate;
		this.minRate = minRate;
		this.kmRate = kmRate;
	}
	/**
	 * @return the parking
	 */
	public Parking getParking() {
		return parking;
	}
	/**
	 * @return the licencePlate
	 */
	public String getLicencePlate() {
		return licencePlate;
	}
	/**
	 * @return the minRate
	 */
	public double getMinRate() {
		return minRate;
	}
	/**
	 * @return the kmRate
	 */
	public double getKmRate() {
		return kmRate;
	}
	/**
	 * @return the reserved
	 */
	public boolean isReserved() {
		return reserved;
	}
	/**
	 * @param reserved the reserved to set
	 */
	public void reserve() {
		this.reserved = true;
	}
	
	public void release() {
		this.reserved = false;
	}
	
	public void use() {
		inUse = true;
		this.reserved = false;
	}
	
	public boolean isInUse() {
		return inUse;
	}
	
	public void terminate() {
		inUse = false;
		this.reserved = false;
	}
	
	public void setParking(Parking parking) {
		this.parking = parking;
		
	}
	
}
