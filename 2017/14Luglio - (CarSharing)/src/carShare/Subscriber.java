package carShare;

import java.util.LinkedList;
import java.util.List;

public class Subscriber {
	private String card;
	private boolean reserved = false;
	private Car reservedCar = null;
	private List<Note> notes = new LinkedList<>();
	
	/**
	 * @param card
	 */
	public Subscriber(String card) {
		this.card = card;
	}

	/**
	 * @return the card
	 */
	public String getCard() {
		return card;
	}

	public boolean hasReserved() {
		return reserved;
	}
	
	public void reserve(Car c) {
		reservedCar = c;
		reserved = true;
	}
	
	public Car getReservedCar() {
		return reservedCar;
	}

	public void release() {
		reserved = false;
		reservedCar = null;
	}

	public void addNote(Note note) {
		notes.add(note);
	}
	
	public List<Note> getNotes(){
		return notes;
	}
}
