package carShare;

import java.util.HashSet;
import java.util.Set;

public class Parking {
	private String name;
	private Set<Car> cars = new HashSet<>();
	
	/**
	 * @param name
	 */
	public Parking(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void addCar(Car c) {
		cars.add(c);
	}

	/**
	 * @return the set of cars
	 */
	public Set<Car> getCars() {
		return cars;
	}

	public void removeCar(Car c) {
		cars.remove(c);
	}

}
