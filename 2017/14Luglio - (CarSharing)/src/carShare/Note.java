package carShare;

public class Note {
	private String card;
	private String plate;
	private String startParking;
	private String destParking;
	private double price;
	/**
	 * @param card
	 * @param plate
	 * @param startParking
	 * @param destParking
	 */
	public Note(String card, String plate, double price,  String startParking, String destParking) {
		this.card = card;
		this.plate = plate;
		this.price = price;
		this.startParking = startParking;
		this.destParking = destParking;
	}
	/**
	 * @return the card
	 */
	public String getCard() {
		return card;
	}
	/**
	 * @return the plate
	 */
	
	public double getPrice() {
		return price;
	}
	
	public String getPlate() {
		return plate;
	}
	/**
	 * @return the startParking
	 */
	public String getStartParking() {
		return startParking;
	}
	/**
	 * @return the destParking
	 */
	public String getDestParking() {
		return destParking;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return card + ":" + plate + ":" + price + ":" + startParking + ":"
				+ destParking;
	}
	
	

}
