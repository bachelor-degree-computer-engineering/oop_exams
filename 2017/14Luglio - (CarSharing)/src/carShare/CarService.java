package carShare;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;

public class CarService {
	private HashMap<String, Parking> parkings = new HashMap<>();
	private HashMap<String, Car> cars = new HashMap<>();
	private TreeMap<String, Subscriber> subscribers = new TreeMap<>();
	private List<Note> notes = new LinkedList<>();
	
	public void addParking(String name) throws InvalidName{
		if(parkings.containsKey(name))
			throw new InvalidName();
		parkings.put(name, new Parking(name));
	}

	public void addCar(String parking, String licencePlate, double minRate, double kmRate) throws InvalidName{
		if(cars.containsKey(licencePlate))
			throw new InvalidName();
		if(!parkings.containsKey(parking))
			throw new InvalidName();
		Parking p = parkings.get(parking);
		Car c = new Car(p, licencePlate, minRate, kmRate);
		p.addCar(c);
		cars.put(licencePlate, c);
	}
		
	public SortedSet<String> getAvailableCars(String parking) throws InvalidName{
		if(!parkings.containsKey(parking))
			throw new InvalidName();
		return parkings.get(parking).getCars().stream()
				.filter(c -> !c.isReserved())
				.map(Car::getLicencePlate)
				.sorted()
				.collect(toCollection(TreeSet::new));
	}
	
	public void addSubscriber(String card) throws InvalidName{
		if(subscribers.containsKey(card))
			throw new InvalidName();
		subscribers.put(card, new Subscriber(card));
	}
	
	public List<String> getSubscribers(){
		return subscribers.values().stream()
				.map(Subscriber::getCard)
				.collect(toList());
	}
	
	public String reserve(String card, String parking) throws InvalidName{
		if(!subscribers.containsKey(card))
			throw new InvalidName();
		if(!parkings.containsKey(parking))
			throw new InvalidName();
		Subscriber s = subscribers.get(card);
		Car c = parkings.get(parking).getCars().stream()
				.filter((Car car) ->!car.isReserved())
				.sorted(comparing(Car::getLicencePlate))
				.findFirst().orElse(null);
		if(c == null)
			return null;
		c.reserve();
		if(s.hasReserved())
			return null;
		s.reserve(c);
		return c.getLicencePlate();
	}
	
	public String release(String card, String plate) throws InvalidName{
		if(!subscribers.containsKey(card))
			throw new InvalidName();
		if(!cars.containsKey(plate))
			throw new InvalidName();
		Subscriber s = subscribers.get(card);
		if(!s.hasReserved())
			return null;
		if(!s.getReservedCar().getLicencePlate().equals(plate))
			return null;
		s.release();
		cars.get(plate).release();
		return plate;
	}
	
	public Set<String> getReserved(String parking) throws InvalidName{
		if(!parkings.containsKey(parking))
			throw new InvalidName();
		return parkings.get(parking).getCars().stream()
				.filter(Car::isReserved)
				.map(Car::getLicencePlate)
				.sorted()
				.collect(toCollection(TreeSet::new));
	}
	
	public String useCar(String card, String plate) throws InvalidName{
		if(!subscribers.containsKey(card))
			throw new InvalidName();
		if(!cars.containsKey(plate))
			throw new InvalidName();
		Subscriber s = subscribers.get(card);
		if(!s.hasReserved())
			return null;
		if(!s.getReservedCar().getLicencePlate().equals(plate))
			return null;
		cars.get(plate).use();
		return plate;
	}
	
	public String terminate(String card, String plate, String parking, int min, int km) throws InvalidName{
		if(!subscribers.containsKey(card))
			throw new InvalidName();
		if(!cars.containsKey(plate))
			throw new InvalidName();
		if(!parkings.containsKey(parking))
			throw new InvalidName();
		
		Subscriber s = subscribers.get(card);
		Car c = cars.get(plate);
		if(!s.hasReserved() || !c.isInUse() ||  !s.getReservedCar().getLicencePlate().equals(plate))
			return null;
		
		c.terminate();
		c.release();
		s.release();
		
		parkings.get(parking).addCar(c);
		
		double price = c.getKmRate() * km + c.getMinRate() * min;
		Note note = new Note(card, plate, price, c.getParking().getName(), parking);
		
		c.getParking().removeCar(c);
		c.setParking(parkings.get(parking));
		
		notes.add(note);
		s.addNote(note);
		return note.toString();
	}

	public List<String> charges() {
		return notes.stream()
				.sorted(comparing(Note::getPrice, reverseOrder()))
				.map(Note::toString)
				.collect(toList());
	}
		
	public List<String> subscriberCharges(String card) throws InvalidName{
		if(!subscribers.containsKey(card))
			throw new InvalidName();
		return subscribers.get(card).getNotes().stream().map(Note::toString).collect(toList());
	}
	
	public double averageCharge() {
		return notes.stream().mapToDouble(Note::getPrice).average().getAsDouble();
	}
	
	public long departuresFrom(String parking) throws InvalidName{
		if(!parkings.containsKey(parking))
			throw new InvalidName();
		return notes.stream()
				.filter(n -> n.getStartParking().equals(parking))
				.count();
	}
}
