package warehouse;

import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Supplier {
	private String code, name;
	private TreeSet<Product> supply = new TreeSet<>(Comparator.comparing(Product::getDescription));
	
	public Supplier(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCodice(){
		return code;
	}

	public String getNome(){
		return name;
	}
	
	public void newSupply(Product product){
		supply.add(product);
		product.addSupplier(this);
	}
	
	public List<Product> supplies(){
		return supply.stream().collect(Collectors.toList());
	}
}
