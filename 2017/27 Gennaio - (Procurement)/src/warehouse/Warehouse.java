package warehouse;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Warehouse {
	private Map<String, Product> products = new HashMap<>();
	private Map<String, Supplier> suppliers = new HashMap<>();
	private Map<String, Order> orders = new HashMap<>();
	private int next = 1;
	
	public Product newProduct(String code, String description){
		Product tmp = new Product(this, code, description);
		products.put(code, tmp);
		return tmp;
	}
	
	public Collection<Product> products(){
		return products.values();
	}

	public Product findProduct(String code){
		return products.get(code);
	}

	public Supplier newSupplier(String code, String name){
		Supplier tmp = new Supplier(code, name);
		suppliers.put(code, tmp);
		return tmp;
	}
	
	public Supplier findSupplier(String code){
		return suppliers.get(code);
	}

	public Order issueOrder(Product prod, int quantity, Supplier supp)
		throws InvalidSupplier {
		if(!supp.supplies().contains(prod))
			throw new InvalidSupplier();
		Order tmp = new Order("ORD"+next++, prod, supp, quantity);
		orders.put(tmp.getCode(), tmp);
		return tmp;
	}

	public Order findOrder(String code){
		return orders.get(code);
	}
	
	public List<Order> pendingOrders(){
		return orders.values().stream().filter(x ->!x.delivered())
				.sorted(Comparator.comparing(o -> o.getProduct().getCode())).
				collect(Collectors.toList());
	}

	public Map<String,List<Order>> ordersByProduct(){
	    return orders.values().stream().collect(
	    		Collectors.groupingBy(
	    				o -> o.getProduct().getCode(),
	    				TreeMap::new,
	    				Collectors.toList()
	    				)
	    		);
	}
	
	public Map<String,Long> orderNBySupplier(){
	    return orders.values().stream().filter(Order::delivered).
	    		collect(Collectors.groupingBy(
	    					o -> o.getSupplier().getNome(),
	    					TreeMap::new,
	    					Collectors.counting()
	    				)
	    		);
	}
	
	public List<String> countDeliveredByProduct(){
	    return orders.values().stream().filter(Order::delivered)
	    		.collect(Collectors.groupingBy(
	    				o -> o.getProduct().getCode(),
	    				Collectors.counting()
	    				)
	    			).entrySet().stream()
	    		.sorted(comparing(Map.Entry<String, Long>::getValue).reversed())
                .map(e -> e.getKey() + " - " + e.getValue())
                .collect(toList());
	}
}
