package warehouse;

public class Order {
	private String code;
	private Product product;
	private Supplier supplier;
	private int quantity;
	private boolean delivered;
	
	public Order(String code, Product product, Supplier supplier, int quantity) {
		this.code = code;
		this.product = product;
		this.supplier = supplier;
		this.quantity = quantity;
		this.delivered = false;
	}

	public String getCode(){
		return code;
	}
	
	public boolean delivered(){
		return delivered;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setDelivered() throws MultipleDelivery {
		if(delivered)
			throw new MultipleDelivery();
		else
			delivered = true;
		int tmp = product.getQuantity();
		product.setQuantity(tmp + quantity);
		
	}
	
	public String toString(){
		return "Order " + code + " for " + quantity + " of "
	           + product.getCode() + " : " + product.getDescription() + " from " +
			   supplier.getNome();	
	}

	public Product getProduct() {
		return product;
	}

	public Supplier getSupplier() {
		return supplier;
	}
}
