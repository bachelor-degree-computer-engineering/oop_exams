package warehouse;

import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Product {
	private String code, Description;
	private int quantity;
	private TreeSet<Supplier> suppliers = new TreeSet<>(Comparator.comparing(Supplier::getNome));
	private Warehouse wh;
	
	public Product(Warehouse wh, String code, String description) {
		this.wh = wh;
		this.code = code;
		Description = description;
		this.quantity = 0;
	}

	public String getCode(){
		return code;
	}

	public String getDescription(){
		return Description;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public void decreaseQuantity(){
		this.quantity--;
	}

	public int getQuantity(){
		return quantity;
	}
	
	public void addSupplier(Supplier s) {
		suppliers.add(s);
	}
	
	public List<Supplier> suppliers(){
		return suppliers.stream().collect(Collectors.toList());
	}
	public boolean equals(Product t) {
		return t.getCode().equals(this.getCode());
	}
	public List<Order> pendingOrders(){
		return wh.pendingOrders().stream().
				filter(o -> o.getProduct().equals(this)).
				sorted(Comparator.comparing(Order::getQuantity, Comparator.reverseOrder())).
				collect(Collectors.toList());
	}
}
