package applications;

import java.util.TreeMap;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

import java.util.Collection;
import java.util.List;

public class Applicant {
	private String name;
	private TreeMap<Skill, Integer> capabilities = new TreeMap<>(comparing(Skill::getName));
	private Position toWantPosition;
	private boolean hasApplication = false;
	/**
	 * @param name
	 */
	public Applicant(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the capabilities
	 */
	public String getCapabilities() {
		if(capabilities.values().size() == 0)
			return "";
		return capabilities.entrySet().stream()
				.map(e -> e.getKey().getName() + ":" + e.getValue())
				.collect(joining(","));
	}

	public void setCapabilities(Skill skill, Integer value) {
		capabilities.put(skill, value);
	}

	public void applicate(Position p) {
		toWantPosition = p;
		hasApplication = true;
	}

	/**
	 * @return the toWantPosition
	 */
	public Position getToWantPosition() {
		return toWantPosition;
	}

	/**
	 * @return the hasApplication
	 */
	public boolean hasApplication() {
		return hasApplication;
	}

	public int getScore() {
		return capabilities.entrySet().stream()
				.filter(e ->toWantPosition.getSkills().contains(e.getKey()))
				.mapToInt(e -> e.getValue())
				.sum();
	}

	public Collection<Skill> getSkills() {
		return capabilities.keySet();
	}
	
	
	
	
}
