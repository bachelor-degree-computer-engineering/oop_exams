package applications;

import java.util.*;
import java.util.stream.Collectors;
import static java.util.Comparator.*;

public class HandleApplications {
	
	private HashMap<String, Skill> skills = new HashMap<>();
	private HashMap<String, Position> positions = new HashMap<>();
	private HashMap<String, Applicant> applicants = new HashMap<>();

	public void addSkills(String... names) throws ApplicationException {
		for(String skill:names) {
			if(skills.containsKey(skill))
				throw new ApplicationException();
			skills.put(skill, new Skill(skill));
		}
	}
	
	public void addPosition(String name, String... skillNames) throws ApplicationException {
		if(positions.containsKey(name))
			throw new ApplicationException();
		Position p = new Position(name);
		positions.put(name, p);
		for(String skill:skillNames) {
			Skill s = skills.get(skill);
			if(s == null)
				throw new ApplicationException();
			s.addPosition(p);
			p.addSkill(s);
		}
	}
	
	public Skill getSkill(String name) {
		return skills.get(name);
	}
	public Position getPosition(String name) {
		return positions.get(name);
	}
	
	public void addApplicant(String name, String capabilities) throws ApplicationException {
		if(applicants.containsKey(name))
			throw new ApplicationException();
		Applicant a = new Applicant(name);
		applicants.put(name, a);
		String[] tmpCapabilities = capabilities.split(",");
		for(String cap: tmpCapabilities) {
			String[] c = cap.split(":");
			if(!skills.containsKey(c[0]))
				throw new ApplicationException();
			Integer value = Integer.parseInt(c[1]);
			if(value < 1 || value > 10) {
				throw new ApplicationException();
			}
			a.setCapabilities(skills.get(c[0]), value);
		}
		
	}
	public String getCapabilities(String applicantName) throws ApplicationException {
		if(!applicants.containsKey(applicantName))
			throw new ApplicationException();	
		return applicants.get(applicantName).getCapabilities();
	}
	
	public void enterApplication(String applicantName, String positionName) throws ApplicationException {
		if(!applicants.containsKey(applicantName))
			throw new ApplicationException();
		if(!positions.containsKey(positionName))
			throw new ApplicationException();
		Position p = positions.get(positionName);
		Applicant a = applicants.get(applicantName);
		if(a.hasApplication())
			throw new ApplicationException();
		List<String> skills = p.getSkills().stream().map(Skill::getName).collect(Collectors.toList());
		String c = a.getCapabilities();
		for(String s:skills) {
			if(!c.contains(s))
				throw new ApplicationException();
		}
		a.applicate(p);
		p.addApplicant(a);
		
	}
	
	public int setWinner(String applicantName, String positionName) throws ApplicationException {
		Applicant a = applicants.get(applicantName);
		Position p = positions.get(positionName);
		if(!a.hasApplication())
			throw new ApplicationException();
		if(!a.getToWantPosition().getName().equals(positionName))
			throw new ApplicationException();
		if(p.hasWinner())
			throw new ApplicationException();
		int score = a.getScore();
		if(score <= 6 * p.getSkills().size())
			throw new ApplicationException();
		p.setWinner(a);
		return score;
	}
	
	public SortedMap<String, Long> skill_nApplicants() {
		return applicants.values().stream()
				.flatMap(a -> a.getSkills().stream())
				.collect(Collectors.groupingBy(
						Skill::getName,
						TreeMap::new,
						Collectors.counting()
					)
				);
	}
	public String maxPosition() {
		return positions.values().stream()
				.max(comparingInt((Position p ) -> p.getApplicants().size()).thenComparing(Position::getName))
				.get().getName();
	}
}

