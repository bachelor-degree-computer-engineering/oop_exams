package applications;

import java.util.*;
import java.util.stream.Collectors;

public class Position {
	private String name;
	private HashSet<Skill> skills = new HashSet<>();
	private TreeSet<Applicant> applicants = new TreeSet<>(Comparator.comparing(Applicant::getName));
	private boolean hasWinner;
	private Applicant winner;
	/**
	 * @param name
	 */
	public Position(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public List<String> getApplicants() {
		return applicants.stream().map(Applicant::getName).collect(Collectors.toList());
	}
	
	public String getWinner() {
		if(!hasWinner)
			return null;
		return winner.getName();
	}

	public void addSkill(Skill s) {
		skills.add(s);
	}

	/**
	 * @return the skills
	 */
	public HashSet<Skill> getSkills() {
		return skills;
	}

	public void addApplicant(Applicant a) {
		applicants.add(a);
	}

	public boolean hasWinner() {
		return hasWinner;
	}

	public void setWinner(Applicant a) {
		winner = a;
		hasWinner = true;
	}
	
	
}