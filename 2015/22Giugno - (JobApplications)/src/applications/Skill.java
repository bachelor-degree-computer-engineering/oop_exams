package applications;

import java.util.*;
import static java.util.Comparator.*;

public class Skill {
	private String name;
	private TreeSet<Position> positions = new TreeSet<>(comparing(Position::getName));
	/**
	 * @param name
	 */
	public Skill(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Position> getPositions() {
		return new ArrayList<Position>(positions);
	}

	public void addPosition(Position p) {
		positions.add(p);	
	}
	
	public boolean equals(Skill o) {
		return o.getName().equals(name);
	}
}