package ticketing;

/**
 * Class representing the ticket linked to an issue or malfunction.
 * 
 * The ticket is characterized by a severity and a state.
 */
public class Ticket {
	
	private int id;
	private User author;
	private String componentPath;
	private String description; 
	private Severity severity;
	
	private User assignedMainteiner;
	private String solDescription;
	private State state = State.Open;
	
	
    /**
	 * @param id
	 * @param author
	 * @param componentPath
	 * @param description
	 * @param severity
	 */
	public Ticket(int id, User author, String componentPath, String description, Severity severity) {
		this.id = id;
		this.author = author;
		this.componentPath = componentPath;
		this.description = description;
		this.severity = severity;
	}

	/**
     * Enumeration of possible severity levels for the tickets.
     * 
     * Note: the natural order corresponds to the order of declaration
     */
    public enum Severity { Blocking, Critical, Major, Minor, Cosmetic };
    
    /**
     * Enumeration of the possible valid states for a ticket
     */
    public static enum State { Open, Assigned, Closed }
    
    public int getId(){
        return id;
    }

    public String getDescription(){
        return description;
    }
    
    public Severity getSeverity() {
        return severity;
    }

    public String getAuthor(){
        return author.getUsername();
    }
    
    public String getComponent(){
        return componentPath;
    }
    
    public State getState(){
        return state;
    }
    
    public String getSolutionDescription() throws TicketException {
    	if(!state.equals(State.Closed))
    		throw new TicketException();
        return solDescription;
    }

	public void assign(User u) {
		this.state = State.Assigned;
		assignedMainteiner = u;
	}

	public void close(String description) {
		solDescription = description;
		this.state = State.Closed;
	}

	public User getManteiner() {
		return this.assignedMainteiner;
		
	}
}
