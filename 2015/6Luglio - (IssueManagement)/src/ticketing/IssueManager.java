package ticketing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Stream;

import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

public class IssueManager {
	private HashMap<String, User> users = new HashMap<>();
	private HashMap<String, Component> components = new HashMap<>(); //the key is the path of the component
	private HashMap<Integer, Ticket> tickets = new HashMap<>();
	
	private int ticketID = 1;
	
    /**
     * Eumeration of valid user classes
     */
    public static enum UserClass {
        /** user able to report an issue and create a corresponding ticket **/
        Reporter, 
        /** user that can be assigned to handle a ticket **/
        Maintainer }
    
    /**
     * Creates a new user
     * 
     * @param username name of the user
     * @param classes user classes
     * @throws TicketException if the username has already been created or if no user class has been specified
     */
    public void createUser(String username, UserClass... classes) throws TicketException {
        if(users.containsKey(username))
        	throw new TicketException();
        if(classes.length == 0)
        	throw new TicketException();
        Set<UserClass> roleSet = Stream.of(classes).collect(toSet());
        users.put(username, new User(username, roleSet));
    }

    /**
     * Creates a new user
     * 
     * @param username name of the user
     * @param classes user classes
     * @throws TicketException if the username has already been created or if no user class has been specified
     */
    public void createUser(String username, Set<UserClass> classes) throws TicketException {
        if(users.containsKey(username))
        	throw new TicketException();
        if(classes.size() == 0)
        	throw new TicketException();
        users.put(username, new User(username, classes));
    }
   
    /**
     * Retrieves the user classes for a given user
     * 
     * @param username name of the user
     * @return the set of user classes the user belongs to
     */
    public Set<UserClass> getUserClasses(String username){
        return users.get(username).getRoles();
    }
    
    /**
     * Creates a new component
     * 
     * @param name unique name of the new component
     * @throws TicketException if a component with the same name already exists
     */
    public void defineComponent(String name) throws TicketException {
        if(components.containsKey("/" + name))
        	throw new TicketException();
        components.put("/" + name, new Component(name));
    }
    
    /**
     * Creates a new sub-component as a child of an existing parent component
     * 
     * @param name unique name of the new component
     * @param parentPath path of the parent component
     * @throws TicketException if the the parent component does not exist or 
     *                          if a sub-component of the same parent exists with the same name
     */
    public void defineSubComponent(String name, String parentPath) throws TicketException {
    	if(components.containsKey(parentPath + "/" + name))
    		throw new TicketException();
    	if(!components.containsKey(parentPath))
    		throw new TicketException();
    	Component c = new Component(name);
        components.put(parentPath + "/" + name, c);
        components.get(parentPath).addSubComponent(c);
    }
    
    /**
     * Retrieves the sub-components of an existing component
     * 
     * @param path the path of the parent
     * @return set of children sub-components
     */
    public Set<String> getSubComponents(String path){
        return components.get(path).getSubComponents().stream().map(Component::getName).collect(toSet());
    }

    /**
     * Retrieves the parent component
     * 
     * @param path the path of the parent
     * @return name of the parent
     */
    public String getParentComponent(String path){
        String tmp = path.substring(0, path.length() - components.get(path).getName().length() - 1);
        if(tmp.equals(""))
        	return null;
        return tmp;
    }

    /**
     * Opens a new ticket to report an issue/malfunction
     * 
     * @param username name of the reporting user
     * @param componentPath path of the component or sub-component
     * @param description description of the malfunction
     * @param severity severity level
     * 
     * @return unique id of the new ticket
     * 
     * @throws TicketException if the user name is not valid, the path does not correspond to a defined component, 
     *                          or the user does not belong to the Reporter {@link IssueManager.UserClass}.
     */
    public int openTicket(String username, String componentPath, String description, Ticket.Severity severity) throws TicketException {
        if(!users.containsKey(username))
        	throw new TicketException();
        if(!components.containsKey(componentPath))
        	throw new TicketException();
        User u = users.get(username);
        if(!u.getRoles().contains(UserClass.Reporter))
        	throw new TicketException();
        Ticket t = new Ticket(ticketID, u, componentPath, description, severity);
        tickets.put(ticketID, t);
        return ticketID++;
        
    }
    
    /**
     * Returns a ticket object given its id
     * 
     * @param ticketId id of the tickets
     * @return the corresponding ticket object
     */
    public Ticket getTicket(int ticketId){
        return tickets.get(ticketId);
    }
    
    /**
     * Returns all the existing tickets sorted by severity
     * 
     * @return list of ticket objects
     */
    public List<Ticket> getAllTickets(){
        return tickets.values().stream()
        		.sorted(comparing(Ticket::getSeverity))
        		.collect(toList());
    }
    
    /**
     * Assign a maintainer to an open ticket
     * 
     * @param ticketId  id of the ticket
     * @param username  name of the maintainer
     * @throws TicketException if the ticket is in state <i>Closed</i>, the ticket id or the username
     *                          are not valid, or the user does not belong to the <i>Maintainer</i> user class
     */
    public void assingTicket(int ticketId, String username) throws TicketException {
    	if(!users.containsKey(username))
        	throw new TicketException();
    	if(!tickets.containsKey(ticketId))
    		throw new TicketException();
    	User u = users.get(username);
    	if(!tickets.get(ticketId).getState().equals(Ticket.State.Open))
    		throw new TicketException();
    	if(!u.getRoles().contains(UserClass.Maintainer))
    		throw new TicketException();
    	tickets.get(ticketId).assign(u);
    	u.tickets.add(tickets.get(ticketId));
    }

    /**
     * Closes a ticket
     * 
     * @param ticketId id of the ticket
     * @param description description of how the issue was handled and solved
     * @throws TicketException if the ticket is not in state <i>Assigned</i>
     */
    public void closeTicket(int ticketId, String description) throws TicketException {
        if(!tickets.get(ticketId).getState().equals(Ticket.State.Assigned))
        	throw new TicketException();
        tickets.get(ticketId).close(description);
        tickets.get(ticketId).getManteiner().countClosed++;
    }

    /**
     * returns a sorted map (keys sorted in natural order) with the number of  
     * tickets per Severity, considering only the tickets with the specific state.
     *  
     * @param state state of the tickets to be counted, all tickets are counted if <i>null</i>
     * @return a map with the severity and the corresponding count 
     */
    public SortedMap<Ticket.Severity,Long> countBySeverityOfState(Ticket.State state){
       if(state == null)
    	   return tickets.values().stream()
    			   .collect(groupingBy(
    					   Ticket::getSeverity,
    					   TreeMap::new,
    					   counting()
    					)
    			   );
       return  tickets.values().stream()
    		   .filter(t -> t.getState().equals(state))
			   .collect(groupingBy(
					   Ticket::getSeverity,
					   TreeMap::new,
					   counting()
					)
			   );
    }

    /**
     * Find the top maintainers in terms of closed tickets.
     * 
     * The elements are strings formatted as <code>"username:###"</code> where <code>username</code> 
     * is the user name and <code>###</code> is the number of closed tickets. 
     * The list is sorter by descending number of closed tickets and then by username.

     * @return A list of strings with the top maintainers.
     */
    public List<String> topMaintainers(){
        return users.values().stream()
        		.sorted(comparing(User::getCountClosed, reverseOrder())
        				.thenComparing(User::getUsername))
        		.map(u -> u.getUsername() + ":" + u.getCountClosed())
        		.collect(toList());
    }
}
