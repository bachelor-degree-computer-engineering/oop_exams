package ticketing;

import java.util.HashSet;
import java.util.Set;

import ticketing.IssueManager.UserClass;

public class User {
	private String username;
	private Set<UserClass> roles;
	Set<Ticket> tickets = new HashSet<>();
	int countClosed = 0;
	
	/**
	 * @param username
	 * @param roles
	 */
	public User(String username, Set<UserClass> roles) {
		this.username = username;
		this.roles = roles;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @return the roles
	 */
	public Set<UserClass> getRoles() {
		return roles;
	}
	
	Set<Ticket> getTickets(){
		return tickets;
	}
	
	public int getCountClosed() {
		return countClosed;
	}
}
