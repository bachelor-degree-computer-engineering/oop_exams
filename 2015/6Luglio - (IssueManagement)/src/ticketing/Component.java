package ticketing;

import java.util.HashSet;
import java.util.Set;

public class Component {
	private String name;
	private Set<Component> subComponents = new HashSet<>();

	/**
	 * @param name
	 */
	public Component(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void addSubComponent(Component c) {
		subComponents.add(c);
	}

	/**
	 * @return the subComponents
	 */
	public Set<Component> getSubComponents() {
		return subComponents;
	}
}
