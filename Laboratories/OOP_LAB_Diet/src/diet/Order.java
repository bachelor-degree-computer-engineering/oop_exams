package diet;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents an order in the take-away system
 */
public class Order {
	private Food food;
	private Restaurant restaurant;
	private User user;
	private String orderHour;
	private OrderStatus orderStatus;
	private PaymentMethod paymentMethod;
	private Map<Menu, Integer> orderedMenus = new TreeMap<>(Comparator.comparing(Menu::getName));
	
	public Order(Food food,User user, Restaurant restaurant, String orderHour, OrderStatus orderStatus,
			PaymentMethod paymentMethod) {
		this.food = food;
		this.restaurant = restaurant;
		this.user = user;
		this.orderHour = getHour(orderHour);
		this.orderStatus = orderStatus;
		this.paymentMethod = paymentMethod;
	}

	private String getHour(String orderHour) {
		String tmp = null;
		String[] hourString = restaurant.getHours();
		LocalTime[] hour = new LocalTime[hourString.length];
		
		for(int i=0; i<hourString.length; i++)
			hour[i] = LocalTime.parse(hourString[i]);
		Arrays.sort(hour);
	
		LocalTime actual = LocalTime.parse(orderHour);
		for(int i=0; i<hour.length; i+=2) {
			if((actual.isAfter(hour[i]) || actual.equals(hour[i])) && actual.isBefore(hour[i+1]))
				tmp = orderHour;
		}
		if(tmp == null) {
			if(actual.isBefore(hour[0]))
				tmp = hour[0].toString();
			for(int i=1; i<hour.length-1; i+=2) {
				if(actual.isAfter(hour[i]) && actual.isBefore(hour[i+1]))
					tmp = hour[i+1].toString();
			}
		}
		return tmp;
	}

	public Order(Food food, User user,  Restaurant restaurant, String orderHour) {
		this.food = food;
		this.user = user;
		this.restaurant = restaurant;
		this.orderHour = getHour(orderHour);
		this.orderStatus = OrderStatus.ORDERED;
		this.paymentMethod = PaymentMethod.CASH;
	}

	/**
	 * Defines the possible order status
	 */
	public enum OrderStatus {
		ORDERED, READY, DELIVERED;
	}
	/**
	 * Defines the possible valid payment methods
	 */
	public enum PaymentMethod {
		PAID, CASH, CARD;
	}
		
	/**
	 * Total order price
	 * @return order price
	 */
	public double Price() {
		return -1.0;
	}
	
	/**
	 * define payment method
	 * 
	 * @param method payment method
	 */
	public void setPaymentMethod(PaymentMethod method) {
		this.paymentMethod = method;
	}
	
	/**
	 * get payment method
	 * 
	 * @return payment method
	 */
	public PaymentMethod getPaymentMethod() {
		return this.paymentMethod;
	}
	
	/**
	 * change order status
	 * @param newStatus order status
	 */
	public void setStatus(OrderStatus newStatus) {
		this.orderStatus = newStatus;
	}
	
	/**
	 * get current order status
	 * @return order status
	 */
	public OrderStatus getStatus(){
		return this.orderStatus;
	}
	
	/**
	 * Add a new menu with the relative order to the order.
	 * The menu must be defined in the {@link Food} object
	 * associated the restaurant that created the order.
	 * 
	 * @param menu     name of the menu
	 * @param quantity quantity of the menu
	 * @return this order to enable method chaining
	 */
	public Order addMenus(String menu, int quantity) {
		orderedMenus.put(restaurant.getMenu(menu), quantity);
		return this;
	}
	
	
	
	public Restaurant getRestaurant() {
		return restaurant;
	}


	public User getUser() {
		return user;
	}

	/**
	 * Converts to a string as:
	 * <pre>
	 * RESTAURANT_NAME, USER_FIRST_NAME USER_LAST_NAME : DELIVERY(HH:MM):
	 * 	MENU_NAME_1->MENU_QUANTITY_1
	 * 	...
	 * 	MENU_NAME_k->MENU_QUANTITY_k
	 * </pre>
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");
		sb.append(restaurant.getName()).append(", ").
		   append(user.getFirstName()).append(" ").
		   append(user.getLastName()).append(" : (").append(orderHour).append("):\n");
		for(Menu m:orderedMenus.keySet())
			sb.append("\t").append(m.getName()).append("->").append(orderedMenus.get(m)).append("\n");
		return sb.toString();
	}

	public String getDeliveryHour() {
		return orderHour;
	}
	
}
