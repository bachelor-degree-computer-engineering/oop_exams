package diet;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents a recipe of the diet.
 * 
 * A recipe consists of a a set of ingredients that are given amounts of raw materials.
 * The overall nutritional values of a recipe can be computed
 * on the basis of the ingredients' values and are expressed per 100g
 * 
 *
 */
public class Recipe implements NutritionalElement {
	
    private Food food;
	private String name;
	private List<Ingredient> ingredients;
	private double actualQuantity;
	private double totalCalories, totalProteins, totalCarbs, totalFat;
	
	public Recipe(Food food,String name) {
		this.food = food;
		this.name = name;
		this.ingredients = new LinkedList<>();
		actualQuantity = 0;
	}

	/**
	 * Adds a given quantity of an ingredient to the recipe.
	 * The ingredient is a raw material.
	 * 
	 * @param material the name of the raw material to be used as ingredient
	 * @param quantity the amount in grams of the raw material to be used
	 * @return the same Recipe object, it allows method chaining.
	 */
	public Recipe addIngredient(String material, double quantity) {
		ingredients.add(new Ingredient(material, quantity));
		totalCalories  += food.getRawMaterial().get(material).getCalories() * (quantity/100);
		totalProteins  += food.getRawMaterial().get(material).getProteins() * (quantity/100);
		totalCarbs     += food.getRawMaterial().get(material).getCarbs()    * (quantity/100);
		totalFat       += food.getRawMaterial().get(material).getFat()      * (quantity/100);
		actualQuantity += quantity;
		return this;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getCalories() {
		return totalCalories * (100/actualQuantity);
	}

	@Override
	public double getProteins() {
		return totalProteins * (100/actualQuantity);
	}

	@Override
	public double getCarbs() {
		return totalCarbs * (100/actualQuantity);
	}

	@Override
	public double getFat() {
		return totalFat * (100/actualQuantity);
	}
	
	/**
	 * Indicates whether the nutritional values returned by the other methods
	 * refer to a conventional 100g quantity of nutritional element,
	 * or to a unit of element.
	 * 
	 * For the {@link Recipe} class it must always return {@code true}:
	 * a recipe expresses nutritional values per 100g
	 * 
	 * @return boolean indicator
	 */
	@Override
	public boolean per100g() {
		return true;
	}
	
	
	/**
	 * Returns the ingredients composing the recipe.
	 * 
	 * A string that contains all the ingredients, one per per line, 
	 * using the following format:
	 * {@code "Material : ###.#"} where <i>Material</i> is the name of the 
	 * raw material and <i>###.#</i> is the relative quantity. 
	 * 
	 * Lines are all terminated with character {@code '\n'} and the ingredients 
	 * must appear in the same order they have been added to the recipe.
	 */
	@Override
	public String toString() {
		StringBuilder sb =  new StringBuilder("");
		for(Ingredient i: ingredients)
			sb.append(i.toString()).append("\n");
		return sb.toString();
	}
}
