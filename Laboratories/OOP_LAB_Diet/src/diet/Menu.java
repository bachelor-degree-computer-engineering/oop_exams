package diet;

/**
 * Represents a complete menu.
 * 
 * It can be made up of both packaged products and servings of given recipes.
 *
 */
public class Menu implements NutritionalElement {
	private String name;
	private double totalCalories, totalProteins, totalCarbs, totalFat;
	private Food food;
	/**
	 * Adds a given serving size of a recipe.
	 * 
	 * The recipe is a name of a recipe defined in the
	 * {@Link Food} in which this menu has been defined.
	 * 
	 * @param recipe the name of the recipe to be used as ingredient
	 * @param quantity the amount in grams of the recipe to be used
	 * @return the same Menu to allow method chaining
	 */
	
	public Menu(Food food, String name) {
		this.name = name;
		this.food = food;
	}
	
	public Menu addRecipe(String recipe, double quantity) {
		totalCalories  += food.getRecipes().get(recipe).getCalories() * (quantity/100);
		totalProteins  += food.getRecipes().get(recipe).getProteins() * (quantity/100);
		totalCarbs     += food.getRecipes().get(recipe).getCarbs()    * (quantity/100);
		totalFat       += food.getRecipes().get(recipe).getFat()      * (quantity/100);
		return this;
	}

	/**
	 * Adds a unit of a packaged product.
	 * The product is a name of a product defined in the
	 * {@Link Food} in which this menu has been defined.
	 * 
	 * @param product the name of the product to be used as ingredient
	 * @return the same Menu to allow method chaining
	 */
	public Menu addProduct(String product) {
		totalCalories  += food.getProduct().get(product).getCalories();
		totalProteins  += food.getProduct().get(product).getProteins();
		totalCarbs     += food.getProduct().get(product).getCarbs();
		totalFat       += food.getProduct().get(product).getFat();
		return this;
	}

	/**
	 * Name of the menu
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * Total KCal in the menu
	 */
	@Override
	public double getCalories() {
		return totalCalories;
	}

	/**
	 * Total proteins in the menu
	 */
	@Override
	public double getProteins() {
		return totalProteins;
	}

	/**
	 * Total carbs in the menu
	 */
	@Override
	public double getCarbs() {
		return totalCarbs;
	}

	/**
	 * Total fats in the menu
	 */
	@Override
	public double getFat() {
		return totalFat;
	}

	/**
	 * Indicates whether the nutritional values returned by the other methods
	 * refer to a conventional 100g quantity of nutritional element,
	 * or to a unit of element.
	 * 
	 * For the {@link Menu} class it must always return {@code false}:
	 * nutritional values are provided for the whole menu.
	 * 
	 * @return boolean 	indicator
	 */
	@Override
	public boolean per100g() {
		// nutritional values are provided for the whole menu.
		return false;
	}
}
