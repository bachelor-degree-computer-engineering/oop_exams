package diet;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import diet.Order.OrderStatus;
import static java.util.Comparator.*;
/**
 * Represents a restaurant in the take-away system
 *
 */
public class Restaurant {
	private String name;
	private Food food;
	private String[] openingHours;
	private Map<String, Menu> menus = new TreeMap<>();
	private List<Order> orders = new LinkedList<>();

	/**
	 * Constructor for a new restaurant.
	 * 
	 * Materials and recipes are taken from
	 * the food object provided as argument.
	 * 
	 * @param name	unique name for the restaurant
	 * @param food	reference food object
	 */
	public Restaurant(String name, Food food) {
		this.name = name;
		this.food = food;
		this.menus = new TreeMap<>();
	}
	
	/**
	 * gets the name of the restaurant
	 * 
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Define opening hours.
	 * 
	 * The opening hours are considered in pairs.
	 * Each pair has the initial time and the final time
	 * of opening intervals.
	 * 
	 * for a restaurant opened from 8:15 until 14:00 and from 19:00 until 00:00, 
	 * is thoud be called as {@code setHours("08:15", "14:00", "19:00", "00:00")}.
	 * 
	 * @param hm a list of opening hours
	 */
	public void setHours(String ... hm) {
		this.openingHours = hm;
	}
	
	public Menu getMenu(String name) {
		return menus.get(name);
	}
	
	/**
	 * Creates a new menu
	 * 
	 * @param name name of the menu
	 * 
	 * @return the newly created menu
	 */
	public Menu createMenu(String name) {
		Menu tmp = new Menu(food, name);
		menus.put(name, tmp);
		return tmp;
	}

	/**
	 * Find all orders for this restaurant with 
	 * the given status.
	 * 
	 * The output is a string formatted as:
	 * <pre>
	 * Napoli, Judi Dench : (19:00):
	 * 	M6->1
	 * Napoli, Ralph Fiennes : (19:00):
	 * 	M1->2
	 * 	M6->1
	 * </pre>
	 * 
	 * The orders are sorted by name of restaurant, name of the user, and delivery time.
	 * 
	 * @param status the status of the searched orders
	 * 
	 * @return the description of orders satisfying the criterion
	 */
	public String ordersWithStatus(OrderStatus status) {
		return orders.stream().
				filter(s -> s.getStatus() == status).
				sorted(
						comparing((Order s) -> s.getRestaurant().getName()).
						thenComparing((Order s) -> s.getUser().getFirstName()).
						thenComparing((Order s) -> s.getDeliveryHour())
				).
				map(Order::toString).
				collect(Collectors.joining());
	}

	public String[] getHours() {
		return openingHours;
	}

	public Food getFood() {
		return food;
	}
	
	public boolean isOpened(String hour) {
		LocalTime time = LocalTime.parse(hour);
		for(int i=0; i<openingHours.length; i+=2) {
			if(time.isAfter(LocalTime.parse(openingHours[i])) && time.isBefore(LocalTime.parse(openingHours[i+1])))
				return true;
		}
		return false;
	}

	public void addOrder(Order tmp) {
		orders.add(tmp);
	}
}
