package university;

public class Corso {
	private String title;
	private String teacher;
	private Student[] students = new Student[MAX_STUDENTS];
	private int code; 
	
	static final int MAX_STUDENTS = 100;
	private int next = 0;
	
	private int[] marks = new int[MAX_STUDENTS];
	private int nextMark = 0;
	
	public Corso(String title, String teacher, int code){
		this.title = title;
		this.teacher = teacher;
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public String getTeacher() {
		return teacher;
	}
	
	public String toString() {
		return code + "," + title+","+teacher;
	}
	
	public void register(Student student) {
		if(next < MAX_STUDENTS) {
			students[next++] = student;
		}
		else return;
	} 
	
	public String attendes() {
		StringBuffer sb = new StringBuffer("");
		for(Student s : students) {
			if(s != null)
				sb.append(s.toString()).append("\n");
		}
		return sb.toString();
	}
	
	public void registerExam(int mark) {
		if(nextMark < MAX_STUDENTS) {
			marks[nextMark++] = mark;
		}
	}

	public float getAvg() {
		int sum=0;
		if(nextMark == 0)
			return -1;
		else {
			for(int i=0; i< nextMark; i++) {
				sum += marks[i];
			}
		}
		return (float) sum/nextMark;
	}
}
