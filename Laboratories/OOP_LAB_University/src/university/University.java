package university;

/**
 * This class represents a university education system.
 * 
 * It manages students and courses.
 *
 */
public class University {
	
	private String name;
	private String rector;
	static final int MAX_STUDENTS = 1000;
	static final int MAX_COURSES = 50;
	private int nextStudent = 0;
	private int nextCourse = 0;
	static final int BASE_STUDENT_CODE = 10000;
	static final int BASE_COURSE_CODE = 10;
	
	protected Student[] students;
	protected Corso[] courses;
	
	/**
	 * Constructor
	 * @param name name of the university
	 */
	public University(String name){
		this.name = name;
		this.students = new Student[MAX_STUDENTS];
		this.courses = new Corso[MAX_COURSES];
	}
	
	/**
	 * Getter for the name of the university
	 * @return name of university
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Defines the rector for the university
	 * 
	 * @param first
	 * @param last
	 */
	public void setRector(String first, String last){
		this.rector = first + " " + last;
	}
	
	/**
	 * Retrieves the rector of the university
	 * 
	 * @return
	 */
	public String getRector(){
		return this.rector;
	}
	
	/**
	 * Enroll a student in the university
	 * 
	 * @param first first name of the student
	 * @param last last name of the student
	 * @return
	 */
	public int enroll(String first, String last){
		if(nextStudent < MAX_STUDENTS) {
			students[nextStudent++] = new Student(first, last, BASE_STUDENT_CODE + nextStudent);
			return BASE_STUDENT_CODE + nextStudent-1;
		}
		else return -1;
	}
	
	/**
	 * Retrieves the information for a given student
	 * 
	 * @param id the id of the student
	 * @return information about the student
	 */
	public String student(int id){
		if(id-BASE_STUDENT_CODE > nextStudent)
			return "Studente non esistente";
		else return students[id-BASE_STUDENT_CODE].toString();
	}
	
	/**
	 * Activates a new course with the given teacher
	 * 
	 * @param title title of the course
	 * @param teacher name of the teacher
	 * @return the unique code assigned to the course
	 */
	public int activate(String title, String teacher){
		if(nextCourse < MAX_COURSES) {
			courses[nextCourse++] = new Corso(title, teacher, BASE_COURSE_CODE + nextCourse - 1);
			return BASE_COURSE_CODE + nextCourse - 1;
		}
		return -1;
	}
	
	/**
	 * Retrieve the information for a given course
	 * 
	 * @param code unique code of the course
	 * @return information about the course
	 */
	public String course(int code){
		if(code - BASE_COURSE_CODE < nextCourse) {
			return courses[code-BASE_COURSE_CODE].toString();
		}
		else return "Corso non esistente";
	}
	
	/**
	 * Register a student to attend a course
	 * @param studentID id of the student
	 * @param courseCode id of the course
	 */
	public void register(int studentID, int courseCode){
		if(studentID-BASE_STUDENT_CODE >= nextStudent) {
			System.out.println("Studente non esistente.");
			return;
		}
		else if(courseCode-BASE_COURSE_CODE >= nextCourse) {
			System.out.println("Corso non esistente");
			return;
		}
		else {
			students[studentID-BASE_STUDENT_CODE].register(courses[courseCode-BASE_COURSE_CODE]);
			courses[courseCode-BASE_COURSE_CODE].register(students[studentID-BASE_STUDENT_CODE]);
		}
	}
	
	/**
	 * Retrieve a list of attendees
	 * 
	 * @param courseCode unique id of the course
	 * @return list of attendees separated by "\n"
	 */
	public String listAttendees(int courseCode){
		return courses[courseCode-BASE_COURSE_CODE].attendes();
	}

	/**
	 * Retrieves the study plan for a student
	 * 
	 * @param studentID id of the student
	 * @return list of courses the student is registered for
	 */
	public String studyPlan(int studentID){
		return students[studentID-BASE_STUDENT_CODE].studyPlan();
	}
	
	public int getStudentNumber() {
		return nextStudent;
	}
}
