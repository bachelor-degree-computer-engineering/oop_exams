package university;

import static java.util.Comparator.*;
import java.util.*;
import java.util.logging.Logger;

public class UniversityExt extends University {
	
	private final static Logger logger = Logger.getLogger("University");
	
	public UniversityExt(String name) {
		super(name);
		logger.info("Creating extended university object");
	}
	/**
	 * 
	 * @param studentId
	 * @param courseID
	 * @param grade
	 */
	public void exam(int studentId, int courseID, int grade) {
		this.students[studentId - University.BASE_STUDENT_CODE].registerExam(grade);
		this.courses[courseID - University.BASE_COURSE_CODE].registerExam(grade);
		logger.info("Student " + studentId + " took an exam in course " + courseID + " with grade " + grade);
	}
	
	/**
	 * 
	 * @param studentId
	 * @return
	 */
	public String studentAvg(int studentId) {
		StringBuilder sb = new StringBuilder("");
		float avg = this.students[studentId - University.BASE_STUDENT_CODE].getAvg();
		if(avg == -1)
			sb.append("Student ").append(studentId).append(" hasn't taken any exams");
		else 
			sb.append("Student ").append(studentId).append(" : ").append(avg);
		return sb.toString();
	}
	
	/**
	 * 
	 * @param courseId
	 * @return
	 */
	public String courseAvg(int courseId) {
		StringBuilder sb = new StringBuilder("");
		float avg = this.courses[courseId - University.BASE_COURSE_CODE].getAvg();
		if(avg == -1)
			sb.append("No student has taken the exam in ").append(courseId);
		else
			sb.append("The average for the course ").append(this.courses[courseId - University.BASE_COURSE_CODE].getTitle()).append(" is: ").append(avg);
		return sb.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	public String topThreeStudents() {
		StringBuilder sb = new StringBuilder("");
		Student[] tmp = Arrays.copyOf(this.students, this.getStudentNumber());
		Arrays.sort(tmp, comparing(Student::getPoint, reverseOrder()));
		for(int i=0; i<3 && i<this.getStudentNumber(); i++) {
			sb.append("STUDENT ").append(tmp[i].getFirst()).append(" ").append(tmp[i].getLast()).append(" : ").append(tmp[i].getPoint()).append("\n");
		}
		return sb.toString();
	}
	
	@Override
	public int enroll(String first, String last){
		int code = super.enroll(first, last);
		if(code == -1)
			return code;
		else {
			logger.info("New student enrolled: " + code + ", " + first + " " + last);
			return code;
		}
	}
	
	@Override
	public int activate(String title, String teacher) {
		int code = super.activate(title, teacher);
		if(code == -1)
			return code;
		else {
			logger.info("New course activated: " + code + ", " + title + " " + teacher);
			return code;
		}
	}
	
	@Override
	public void register(int studentID, int courseCode) {
		super.register(studentID, courseCode);
		logger.info("Student " + studentID + " signed up for course " + courseCode);
	}
}
