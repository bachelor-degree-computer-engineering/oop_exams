package university;

public class Student{
	
	private String first;
	private String last;
	
	public Student(String first, String last, int code) {
		this.first = first;
		this.last = last;
		this.code = code;
	}

	static final int MAX_COURSES_ATTENDABLE = 25;
	private int next = 0;
	private Corso[] courses = new Corso[MAX_COURSES_ATTENDABLE];
	private int code;	
	private int[] marks = new int[MAX_COURSES_ATTENDABLE];
	private int nextMark = 0;

	public int getCode() {
		return code;
	}
	
	public String toString() {
		return code + " " + first + " " + last;
	}
	
	public void register(Corso course) {
		if(next < MAX_COURSES_ATTENDABLE) {
			courses[next++] = course;
		}
	}

	public String studyPlan() {
		StringBuffer sb = new StringBuffer("");
		for(Corso corso:courses) {
			if(corso != null)
				sb.append(corso.toString()).append("\n");
		}
		return sb.toString();
	}
	
	public void registerExam(int mark) {
		if(nextMark < MAX_COURSES_ATTENDABLE) {
			marks[nextMark++] = mark;
		}
		else
			System.out.println("Non � pi� possibile inserire voti.");
	}

	public String getFirst() {
		return first;
	}

	public String getLast() {
		return last;
	}

	public static int getMaxCoursesAttendable() {
		return MAX_COURSES_ATTENDABLE;
	}

	public float getAvg() {
		int sum=0;
		if(nextMark == 0)
			return -1;
		else {
			for(int i=0; i< nextMark; i++) {
				sum += marks[i];
			}
		}
		return (float) sum/nextMark;
	}
	
	public float getPoint() {
		return this.getAvg() +  10 * (nextMark/next);
	}
}
