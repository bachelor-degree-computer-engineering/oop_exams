package hydraulic;

/**
 * Represents a split element, a.k.a. T element
 * 
 * During the simulation each downstream element will
 * receive a stream that is half the input stream of the split.
 */

public class Split extends ElementExt {
	private Element[] output;
	/**
	 * Constructor
	 * @param name
	 */
	public Split(String name) {
		super(name);
		output = new Element[2];
	}
    
	/**
	 * returns the downstream elements
	 * @return array containing the two downstream element
	 */
    public Element[] getOutputs(){
    	return output;
    }

    /**
     * connect one of the outputs of this split to a
     * downstream component.
     * 
     * @param elem  the element to be connected downstream
     * @param noutput the output number to be used to connect the element
     */
	public void connect(Element elem, int noutput){
		if(noutput == 0 || noutput == 1)
			output[noutput] = elem;
		else {
			System.out.println("noutput out of bound.");
		}
	}
	
	public void simulate(SimulationObserver observer, double inFlow) {
		Element[] output = new Element[2];
		double[] flows = {inFlow/2, inFlow/2};
		observer.notifyFlow("Split", this.name, inFlow, flows);
		output = this.getOutputs();
		if(output[0] !=  null)
			output[0].simulate(observer, inFlow/2);
		if(output[1] != null)
			output[1].simulate(observer, inFlow/2);
	}
	

	public void simulateAndCheck(SimulationObserverExt observer, double inFlow) {
		if(inFlow > this.getMaxFlow() && this.isMaxFlowDefined())
			observer.notifyFlowError("Split", name, inFlow, this.getMaxFlow());
		double[] flows = {inFlow/2, inFlow/2};
		observer.notifyFlow("Split", this.name, inFlow, flows);
		output = this.getOutputs();
		if(output[0] !=  null)
			output[0].simulateAndCheck(observer, inFlow/2);
		if(output[1] != null)
			output[1].simulateAndCheck(observer, inFlow/2);
	}
	
	public String toString() {
		return "["+this.getName()+"]"+"Split";
	}
	
	public void layoutprinting(StringBuilder s, int length) {
		int actualSpace;
		s.append(this.toString());
		actualSpace = length + this.toString().length();
		for(Element e:output) {
			if(e == null)
				s.append(" * ");
			else {
				for(int i=0; i < actualSpace; i++)
					s.append(" ");
				s.append(" |\n");
				for(int i=0; i < actualSpace; i++)
					s.append(" ");
				s.append(" ").append(HSystemExt.SPLIT_CONNECTORS).append(" ");
				e.getOutput().layoutprinting(s, actualSpace);
			}
		}
	}
}
