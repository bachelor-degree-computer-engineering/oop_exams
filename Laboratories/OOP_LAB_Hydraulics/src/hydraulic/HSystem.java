package hydraulic;

import java.util.Arrays;

/**
 * Main class that act as a container of the elements for
 * the simulation of an hydraulics system 
 * 
 */
public class HSystem {
	protected static final int MAX_ELEMENTS = 100;
	
	protected Element[] Elements = new Element[MAX_ELEMENTS];
	protected int nElements = 0;
	
	/**
	 * Adds a new element to the system
	 * @param elem
	 */
	public void addElement(Element elem){
		if(nElements >= MAX_ELEMENTS) {
			System.out.println("No elements can be added.");
		}
		else {
			Elements[nElements++] = elem;
		}
		return;
	}
	
	/**
	 * returns the element added so far to the system.
	 * If no element is present in the system an empty array (length==0) is returned.
	 * 
	 * @return an array of the elements added to the hydraulic system
	 */
	public Element[] getElements(){
		Element[] tmp = new Element[nElements];
		tmp = Arrays.copyOf(Elements, nElements);
		return tmp;
	}
	
	/**
	 * Prints the layout of the system starting at each Source
	 */
	public String layout(){
		// TODO: to be implemented
		return null;
	}
	
	/**
	 * starts the simulation of the system
	 */
	public void simulate(SimulationObserver observer){
		for(Element e:Elements) {
			if(e instanceof Source) {
				e.simulate(observer, 0);
			}
		}
	}

}
