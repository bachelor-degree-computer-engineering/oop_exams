package hydraulic;

/**
 * Represents a tap that can interrupt the flow.
 * 
 * The status of the tap is defined by the method
 * {@link #setOpen(boolean) setOpen()}.
 */

public class Tap extends ElementExt {
	private boolean open;
	
	public Tap(String name) {
		super(name);
		this.open = false;
	}
	
	/**
	 * Defines whether the tap is open or closed.
	 * 
	 * @param open  opening level
	 */
	public void setOpen(boolean open){
		this.open = open;
	}
	
	public void simulate(SimulationObserver observer, double inFlow) {
		if(open) {
			observer.notifyFlow("Tap", this.name, inFlow, inFlow);
			if(this.getOutput() !=  null)
				this.getOutput().simulate(observer, inFlow);
		}
		else {
			observer.notifyFlow("Tap", this.name, inFlow, 0);
			if(this.getOutput() !=  null)
				this.getOutput().simulate(observer, 0);
		}
	}

	public void simulateAndCheck(SimulationObserverExt observer, double inFlow) {
		if(inFlow > this.getMaxFlow() && this.isMaxFlowDefined())
			observer.notifyFlowError("Tap", name, inFlow, this.getMaxFlow());
		if(open) {
			observer.notifyFlow("Tap", this.name, inFlow, inFlow);
			if(this.getOutput() !=  null)
				this.getOutput().simulateAndCheck(observer, inFlow);
		}
		else {
			observer.notifyFlow("Tap", this.name, inFlow, 0);
			if(this.getOutput() !=  null)
				this.getOutput().simulateAndCheck(observer, 0);
		}
	}	
	
	public String toString() {
		return "["+this.getName()+"]"+"Tap";
	}
	
	public void layoutprinting(StringBuilder s, int depth) {	
		s.append(this.toString());
		if(this.getOutput() == null)
			s.append(" * ");
		else {
			s.append(" ").append(HSystemExt.SIMPLE_CONNECTORS).append(" ");
			this.getOutput().layoutprinting(s, depth + this.toString().length() + 4);
		}
	}
}
