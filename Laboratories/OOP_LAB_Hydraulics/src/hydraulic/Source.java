package hydraulic;

/**
 * Represents a source of water, i.e. the initial element for the simulation.
 *
 * The status of the source is defined through the method
 * {@link #setFlow(double) setFlow()}.
 */
public class Source extends ElementExt {
	
	private double outFlow;
	
	public Source(String name) {
		super(name);
	}

	/**
	 * defines the flow produced by the source
	 * 
	 * @param flow
	 */
	public void setFlow(double flow){
		this.outFlow = flow;
	}
	
	public void simulate(SimulationObserver observer, double inFlow) {
		observer.notifyFlow("Source", this.name, SimulationObserver.NO_FLOW, this.getFlow());
		if(this.getOutput() !=  null)
			this.getOutput().simulate(observer, this.getFlow());
	}
	
	public void simulateAndCheck(SimulationObserverExt observer, double inFlow) {
		observer.notifyFlow("Source", this.name, SimulationObserver.NO_FLOW, this.getFlow());
		if(this.getOutput() !=  null)
			this.getOutput().simulateAndCheck(observer, this.getFlow());	
		}
	
	public double getFlow() {
		return outFlow;
	}
	
	public String toString() {
		return "["+this.getName()+"]"+"Source";
	}
	
	public void setMaxFlow(double maxFlow) {
		return;
	}
	
	public void layoutprinting(StringBuilder s, int depth) {	
		s.append(this.toString());
		if(this.getOutput() == null)
			s.append(" * ");
		else {
			s.append(" ").append(HSystemExt.SIMPLE_CONNECTORS).append(" ");
			this.getOutput().layoutprinting(s, depth + this.toString().length() + 4);
		}
	}

	
}
