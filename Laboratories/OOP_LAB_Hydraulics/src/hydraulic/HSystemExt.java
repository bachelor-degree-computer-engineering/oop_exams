package hydraulic;

import java.util.Arrays;

/**
 * Main class that act as a container of the elements for
 * the simulation of an hydraulics system 
 * 
 */
public class HSystemExt extends HSystem{
	static final protected String SIMPLE_CONNECTORS = "->";
	static final protected String SPLIT_CONNECTORS = "+->";
	/**
	 * Prints the layout of the system starting at each Source
	 */
	

	public String layout(){
		StringBuilder s = new StringBuilder("");
		for(Element e:this.getElements())
			if(e instanceof Source)
				e.layoutprinting(s, 0);
		return s.toString();
	}
	
	/**
	 * Deletes a previously added element with the given name from the system
	 */
	public void deleteElement(String name) {
		ElementExt x = null;
		Element[] tmp = super.getElements();
		for(Element e: tmp) {
			if(e.getName().equals(name)) {
				if(e instanceof Split)
					return;
				x = (ElementExt) e;
			}		
		}
		if(x == null)
			return;
		for(Element e: tmp) {
			if(e.isConnected(x));
		}
		for(int i=0; i<MAX_ELEMENTS; i++) {
			if(Elements[i] != null)
				if(Elements[i].getName().equals(name)) {
					Elements[i] = null;
					nElements--;
				}
		}
		return;
	}

	/**
	 * starts the simulation of the system; if enableMaxFlowCheck is true,
	 * checks also the elements maximum flows against the input flow
	 */
	public void simulate(SimulationObserverExt observer, boolean enableMaxFlowCheck) {
		if(!enableMaxFlowCheck)
			super.simulate(observer);
		else {
			for(Element e: super.getElements()) {
				if(e instanceof Source)
					e.simulateAndCheck(observer, 0);
			}
		}
	}
	
}
