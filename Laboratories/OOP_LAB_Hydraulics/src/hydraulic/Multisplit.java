package hydraulic;

import java.util.Arrays;

/**
 * Represents a multisplit element, an extension of the Split that allows many outputs
 * 
 * During the simulation each downstream element will
 * receive a stream that is determined by the proportions.
 */

public class Multisplit extends Split {
	Element[] outputs;
	private int numOutput;
	double[] proportions;
	/**
	 * Constructor
	 * @param name
	 * @param numOutput
	 */
	public Multisplit(String name, int numOutput) {
		super(name); 
		this.numOutput = numOutput;
		this.outputs = new Element[numOutput]; 
		proportions = new double[numOutput];
	}
    
	/**
	 * returns the downstream elements
	 * @return array containing the two downstream element
	 */
    public Element[] getOutputs(){
    	return this.outputs;
    }

    /**
     * connect one of the outputs of this split to a
     * downstream component.
     * 
     * @param elem  the element to be connected downstream
     * @param noutput the output number to be used to connect the element
     */
	public void connect(Element elem, int noutput){
		this.outputs[noutput] = elem;
	}
	
	/**
	 * Define the proportion of the output flows w.r.t. the input flow.
	 * 
	 * The sum of the proportions should be 1.0 and 
	 * the number of proportions should be equals to the number of outputs.
	 * Otherwise a check would detect an error.
	 * 
	 * @param proportions the proportions of flow for each output
	 */
	public void setProportions(double... proportions) {
		this.proportions = Arrays.copyOf(proportions, this.numOutput);
	}
	
	public String toString() {
		return "["+this.getName()+"]"+"MultiSplit";
	}
	
	public void simulate(SimulationObserver observer, double inFlow) {
		double[] flows = new double[numOutput];
		for(int i=0; i<numOutput; i++)
			flows[i] = proportions[i] * inFlow;
		observer.notifyFlow("Multisplit", this.name, inFlow, flows);
		for(int i=0; i<numOutput; i++)
			if(outputs[i] != null)
				outputs[i].simulate(observer, flows[i]);
	}
	
	public void simulateAndCheck(SimulationObserverExt observer, double inFlow) {
		if(inFlow > this.getMaxFlow() && this.isMaxFlowDefined())
			observer.notifyFlowError("Tap", name, inFlow, this.getMaxFlow());
		double[] flows = new double[numOutput];
		for(int i=0; i<numOutput; i++)
			flows[i] = proportions[i] * inFlow;
		observer.notifyFlow("Multisplit", this.name, inFlow, flows);
		for(int i=0; i<numOutput; i++)
			if(outputs[i] != null)
				outputs[i].simulateAndCheck(observer, flows[i]);
	}
	
	public void layoutprinting(StringBuilder s, int depth) {
		int actualSpace;
		s.append(this.toString());
		actualSpace = depth + this.toString().length();
		for(int i=0; i<numOutput; i++) {
			if(outputs[i] == null)
				s.append(" * ");
			else {
				s.append(" ").append(HSystemExt.SPLIT_CONNECTORS).append(" ");
				outputs[i].layoutprinting(s, actualSpace);
				if(i < numOutput-1) {
					for(int j=0; j < actualSpace; j++)
					s.append(" ");
					s.append(" |\n");
					for(int j=0; j < actualSpace; j++)
						s.append(" ");
				}
			}	
		}
	}
	
	@Override
	public boolean isConnected(ElementExt x) {
		for(Element e: outputs) {
			if(e.getName().equals(x.getName())) {
				e = x.getOutput();
				return true;
			}
		}
		return false;
	}
}
