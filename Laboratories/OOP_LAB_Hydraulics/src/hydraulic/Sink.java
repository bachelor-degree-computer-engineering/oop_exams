package hydraulic;

/**
 * Represents the sink, i.e. the terminal element of a system
 *
 */
public class Sink extends ElementExt {

	/**
	 * Constructor
	 * @param name
	 */
	public Sink(String name) {
		super(name);
	}
	
	public void simulate(SimulationObserver observer, double inFlow) {
		observer.notifyFlow("Sink", this.name, inFlow, SimulationObserver.NO_FLOW);
	}
	
	public void simulateAndCheck(SimulationObserverExt observer, double inFlow) {
		if(inFlow > this.getMaxFlow() && this.isMaxFlowDefined())
			observer.notifyFlowError("Tap", name, inFlow, this.getMaxFlow());
		observer.notifyFlow("Sink", name, inFlow, SimulationObserver.NO_FLOW);
	}	
	
	public String toString() {
		return "["+this.getName()+"]"+"Sink *";
	}
	
	public void layoutprinting(StringBuilder s, int depth) {	
		s.append(this.toString());
		if(this.getOutput() == null)
			s.append("\n");
	}
}
