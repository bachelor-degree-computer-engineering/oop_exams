package hydraulic;

public abstract class ElementExt extends Element{
	
	private double maxFlow;
	private boolean isMaxFlowDefined = false;
	
	public ElementExt(String name) {
		super(name);
	}

	public void setMaxFlow(double maxFlow) {
		this.maxFlow = maxFlow;
		isMaxFlowDefined = true;
	}

	public boolean isMaxFlowDefined() {
		return isMaxFlowDefined;
	}

	public double getMaxFlow() {
		return maxFlow;
	}
	
	public boolean isConnected(ElementExt x) {
		if(this.getOutput()!= null)
			if(this.getOutput().getName().equals(x.getName())) {
				this.output = x.getOutput();
				return true;
			}
		return false;
	}
}
