package clinic;

import java.util.HashSet;

public class Doctor extends Patient {
	private int id;
	private String specialization;
	private HashSet<Patient> assignedPatients = new HashSet<>();
	
	public Doctor(String name, String surname, String ssn, int id, String specialization) {
		super(name, surname, ssn);
		this.id = id;
		this.specialization = specialization;
	}

	public int getId() {
		return id;
	}
	
	public int getNP() {
		return assignedPatients.size();
	}
	
	public String getSpecialization() {
		return specialization;
	}
	
	@Override
	public String toString() {
		return super.toString() + " ["+ id + "]: " + specialization;
	}

	public HashSet<Patient> getAssignedPatients() {
		return assignedPatients;
	}

	public void addPatients(Patient assignedPatient) {
		this.assignedPatients.add(assignedPatient);
	}
	
	
}
