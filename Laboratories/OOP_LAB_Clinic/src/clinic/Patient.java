package clinic;

public class Patient {
	private String name;
	private String surname;
	private String ssn;
	private Doctor assignedDoctor;
	
	public Patient(String name, String surname, String ssn) {
		this.name = name;
		this.surname = surname;
		this.ssn = ssn;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getSsn() {
		return ssn;
	}
	
	public String toString() {
		return surname + " " + name + " (" + ssn + ")";
	}

	public Doctor getAssignedDoctor() {
		return assignedDoctor;
	}

	public void setAssignedDoctor(Doctor assignedDoctor) {
		this.assignedDoctor = assignedDoctor;
	}
	
	
}
