package mountainhuts;

/**
 * Represents a municipality
 *
 */
public class Municipality {
	private String name;
	private String province;
	private int altitude;
	private Region r;
	/**
	 * Name of the municipality.
	 * 
	 * Within a region the name of a municipality is unique
	 * 
	 * @return name
	 */
	
	public Municipality(Region r, String name, String province, int altitude) {
		this.name = name;
		this.province = province;
		this.r = r;
		this.altitude = altitude;
	}
	public String getName() {
		return this.name;
	}

	/**
	 * Province of the municipality
	 * 
	 * @return province
	 */
	public String getProvince() {
		return this.province;
	}

	/**
	 * Altitude of the municipality
	 * 
	 * @return altitude
	 */
	public Integer getAltitude() {
		return this.altitude;
	}
	
	public Long getNumberOfMountainHuts() {
		return r.getMountainHuts().stream().filter(h -> h.getMunicipality().equals(this)).count();
	}
}
