package mountainhuts;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class {@code Region} represents the main facade
 * class for the mountains hut system.
 * 
 * It allows defining and retrieving information about
 * municipalities and mountain huts.
 *
 */
public class Region {
	private String name;
	private AltitudeRange[] altitudeRanges;
	private Map<String, Municipality> municipalities = new HashMap<>();
	private Map<String, MountainHut> mountainHuts = new HashMap<>();
	/**
	 * Create a region with the given name.
	 * 
	 * @param name
	 *            the name of the region
	 */
	public Region(String name) {
		this.name = name;
	}

	/**
	 * Return the name of the region.
	 * 
	 * @return the name of the region
	 */
	public String getName() {
		return name;
	}

	/**
	 * Create the ranges given their textual representation in the format
	 * "[minValue]-[maxValue]".
	 * 
	 * @param ranges
	 *            an array of textual ranges
	 */
	public void setAltitudeRanges(String... ranges) {
		altitudeRanges = new AltitudeRange[ranges.length];
		for(int i=0; i <ranges.length; i++)
			altitudeRanges[i] = new AltitudeRange(ranges[i]);
	}

	/**
	 * Return the textual representation in the format "[minValue]-[maxValue]" of
	 * the range including the given altitude or return the default range "0-INF".
	 * 
	 * @param altitude
	 *            the geographical altitude
	 * @return a string representing the range
	 */
	public String getAltitudeRange(Integer altitude) {
		String noRange = "0-INF";
		if(altitudeRanges == null)
			return noRange;
		for(AltitudeRange r:altitudeRanges)
			if(r.isInRange(altitude))
				return r.toString();
		return noRange;
	}

	/**
	 * Create a new municipality if it is not already available or find it.
	 * Duplicates must be detected by comparing the municipality names.
	 * 
	 * @param name
	 *            the municipality name
	 * @param province
	 *            the municipality province
	 * @param altitude
	 *            the municipality altitude
	 * @return the municipality
	 */
	public Municipality createOrGetMunicipality(String name, String province, Integer altitude) {
		if(municipalities.containsKey(name))
			return municipalities.get(name);
		Municipality tmp = new Municipality(this, name, province, altitude);
		municipalities.put(name, tmp);
		return tmp;
	}

	/**
	 * Return all the municipalities available.
	 * 
	 * @return a collection of municipalities
	 */
	public Collection<Municipality> getMunicipalities() {
		return municipalities.values();
	}

	/**
	 * Create a new mountain hut if it is not already available or find it.
	 * Duplicates must be detected by comparing the mountain hut names.
	 *
	 * @param name
	 *            the mountain hut name
	 * @param category
	 *            the mountain hut category
	 * @param bedsNumber
	 *            the number of beds in the mountain hut
	 * @param municipality
	 *            the municipality in which the mountain hut is located
	 * @return the mountain hut
	 */
	public MountainHut createOrGetMountainHut(String name, String category, Integer bedsNumber,
			Municipality municipality) {
		if(mountainHuts.containsKey(name)) {
			return mountainHuts.get(name);
		}
		MountainHut tmp = new MountainHut(this, name, Optional.empty(), category, bedsNumber, municipality);
		mountainHuts.put(name, tmp);
		return tmp;
	}

	/**
	 * Create a new mountain hut if it is not already available or find it.
	 * Duplicates must be detected by comparing the mountain hut names.
	 * 
	 * @param name
	 *            the mountain hut name
	 * @param altitude
	 *            the mountain hut altitude
	 * @param category
	 *            the mountain hut category
	 * @param bedsNumber
	 *            the number of beds in the mountain hut
	 * @param municipality
	 *            the municipality in which the mountain hut is located
	 * @return a mountain hut
	 */
	public MountainHut createOrGetMountainHut(String name, Integer altitude, String category, Integer bedsNumber,
			Municipality municipality) {
		if(mountainHuts.containsKey(name)) {
			return mountainHuts.get(name);
		}
		MountainHut tmp = new MountainHut(this, name, Optional.ofNullable(altitude), category, bedsNumber, municipality);
		mountainHuts.put(name, tmp);
		return tmp;
	}

	/**
	 * Return all the mountain huts available.
	 * 
	 * @return a collection of mountain huts
	 */
	public Collection<MountainHut> getMountainHuts() {
		return mountainHuts.values();
	}

	/**
	 * Factory methods that creates a new region by loadomg its data from a file.
	 * 
	 * The file must be a CSV file and it must contain the following fields:
	 * <ul>
	 * <li>{@code "Province"},
	 * <li>{@code "Municipality"},
	 * <li>{@code "MunicipalityAltitude"},
	 * <li>{@code "Name"},
	 * <li>{@code "Altitude"},
	 * <li>{@code "Category"},
	 * <li>{@code "BedsNumber"}
	 * </ul>
	 * 
	 * The fields are separated by a semicolon (';'). The field {@code "Altitude"}
	 * may be empty.
	 * 
	 * @param name
	 *            the name of the region
	 * @param file
	 *            the path of the file
	 */
	public static Region fromFile(String name, String file) {
		Region tmpRegion = new Region(name);
		List<String> lines = readData(file);
		String[] dataOnLine;
		for(String line:lines) {
			dataOnLine = line.split(";");
			Municipality currentMunicipality = tmpRegion.createOrGetMunicipality(dataOnLine[1], dataOnLine[0], Integer.parseInt(dataOnLine[2]));
			if(dataOnLine[4].isEmpty())
				tmpRegion.createOrGetMountainHut(dataOnLine[3], dataOnLine[5], Integer.parseInt(dataOnLine[6]), currentMunicipality);
			else {
				Integer altitude = Integer.parseInt(dataOnLine[4]);
				tmpRegion.createOrGetMountainHut(dataOnLine[3], altitude, dataOnLine[5], Integer.parseInt(dataOnLine[6]), currentMunicipality);

			}
		}
		return tmpRegion;
	}

	/**
	 * Internal class that can be used to read the lines of
	 * a text file into a list of strings.
	 * 
	 * When reading a CSV file remember that the first line
	 * contains the headers, while the real data is contained
	 * in the following lines.
	 * 
	 * @param file the file name
	 * @return a list containing the lines of the file
	 */
	private static List<String> readData(String file) {
		try (BufferedReader in = new BufferedReader(new FileReader(file))) {
			return in.lines().skip(1).collect(toList());
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Count the number of municipalities with at least a mountain hut per each
	 * province.
	 * 
	 * @return a map with the province as key and the number of municipalities as
	 *         value
	 */
	public Map<String, Long> countMunicipalitiesPerProvince() {
		return  mountainHuts.values().stream().
				map(MountainHut::getMunicipality).distinct().
				collect(groupingBy(
						Municipality::getProvince,
						counting())
			    );
	}

	/**
	 * Count the number of mountain huts per each municipality within each province.
	 * 
	 * @return a map with the province as key and, as value, a map with the
	 *         municipality as key and the number of mountain huts as value
	 */
	public Map<String, Map<String, Long>> countMountainHutsPerMunicipalityPerProvince() {
		return mountainHuts.values().stream().
				map(MountainHut::getMunicipality).distinct().
				collect(groupingBy(
						Municipality::getProvince,
						groupingBy(Municipality::getName,
								counting()
								)
						)
						);
	}

	/**
	 * Count the number of mountain huts per altitude range. If the altitude of the
	 * mountain hut is not available, use the altitude of its municipality.
	 * 
	 * @return a map with the altitude range as key and the number of mountain huts
	 *         as value
	 */
	public Map<String, Long> countMountainHutsPerAltitudeRange() {
		Map<String, Long> res =  mountainHuts.values().stream().
				collect(groupingBy(
						 MountainHut::getAltitudeRange,
						 counting()
						)
				);
		Stream.of(altitudeRanges).map(x->x.toString()).
			forEach( r -> res.putIfAbsent(r, 0L));
		return res;
	}

	/**
	 * Compute the total number of beds available in the mountain huts per each
	 * province.
	 * 
	 * @return a map with the province as key and the total number of beds as value
	 */
	public Map<String, Integer> totalBedsNumberPerProvince() {
		return mountainHuts.values().stream().
				collect(groupingBy(
							h -> h.getMunicipality().getProvince(),
							Collectors.summingInt(MountainHut::getBedsNumber)
						)
				);
	}

	/**
	 * Compute the maximum number of beds available in a single mountain hut per
	 * altitude range. If the altitude of the mountain hut is not available, use the
	 * altitude of its municipality.
	 * 
	 * @return a map with the altitude range as key and the maximum number of beds
	 *         as value
	 */
	public Map<String, Optional<Integer>> maximumBedsNumberPerAltitudeRange() {
		return mountainHuts.values().stream().
				collect(groupingBy(
						MountainHut::getAltitudeRange,
						Collectors.mapping(MountainHut::getBedsNumber, Collectors.maxBy(Comparator.naturalOrder()))
						)
				);
	}

	/**
	 * Compute the municipality names per number of mountain huts in a municipality.
	 * The lists of municipality names must be in alphabetical order.
	 * 
	 * @return a map with the number of mountain huts in a municipality as key and a
	 *         list of municipality names as value
	 */
	public Map<Long, List<String>> municipalityNamesPerCountOfMountainHuts() {
		return mountainHuts.values().stream().
				collect(groupingBy(
					    h -> h.getMunicipality().getNumberOfMountainHuts(),
						Collectors.mapping((MountainHut h) -> h.getMunicipality().getName(), toList())
					    )		
				);
	}

}
