package mountainhuts;

public class AltitudeRange {
	private int minValue, maxValue;
	
	public AltitudeRange(String range) {
		String[] value = range.split("-");
		minValue = Integer.parseInt(value[0]);
		maxValue = Integer.parseInt(value[1]);
	}
	
	public boolean isInRange(Integer altitude) {
		return ((altitude >= minValue) && (altitude <= maxValue));
	}
	
	@Override
	public String toString() {
		return minValue+"-"+maxValue;
	}
}
