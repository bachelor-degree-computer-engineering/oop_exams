package transactions;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Carrier {
	private String name;
	private List<Region> servedRegion = new LinkedList<>();
	
	
	public Carrier(String name) {
		this.name = name;
	}
	
	public void addRegion(Region r) {
		if(!servedRegion.contains(r))
			servedRegion.add(r);
	}
	
	public String getName() {
		return name;
	}
	
	public List<String> getRegion(){
		return servedRegion.stream().map(Region::getName).
				sorted().
				collect(Collectors.toList());
	}
	
	public List<Region> getRegions(){
		return servedRegion;
	}
	
	
}
