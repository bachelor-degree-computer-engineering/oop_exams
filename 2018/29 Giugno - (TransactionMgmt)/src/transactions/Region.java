package transactions;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Region {
	private String name;
	private List<String> placeNames = new LinkedList<>();
	
	public Region(String name) {
		this.name = name;
	}
	
	public void addPlace(String place) {
		placeNames.add(place);
	}
	
	public String getName() {
		return name;
	}
	
	public List<String> getPlaceNames(){
		Collections.sort(placeNames);
		return placeNames;
	}
}

