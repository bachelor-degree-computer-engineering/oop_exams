package transactions;

public class Offer {
	private String id;
	private String placeName;
	private String productId;
	
	public Offer(String id, String placeName, String productId) {
		this.id = id;
		this.placeName = placeName;
		this.productId = productId;
	}

	public String getId() {
		return id;
	}

	public String getPlaceName() {
		return placeName;
	}

	public String getProductId() {
		return productId;
	}
	
	
}
