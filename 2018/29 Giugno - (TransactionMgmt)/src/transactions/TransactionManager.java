package transactions;
import static java.util.Comparator.reverseOrder;

import java.util.*;
import java.util.stream.Collectors;

//import static java.util.stream.Collectors.*;
//import static java.util.Comparator.*;

public class TransactionManager {
	private Map<String, Region> regions = new HashMap<>();
	private Map<String, Carrier> carriers = new HashMap<>();
	private List<String> places = new LinkedList<>();
	
	private Map<String, Request> requests = new HashMap<>();
	private Map<String, Offer> offers = new HashMap<>();
	private Map<String, Transaction> transactions = new HashMap<>();
//R1
	public List<String> addRegion(String regionName, String... placeNames) {
		Region tmp = new Region(regionName);
		for(String s: placeNames) {
			if(!places.contains(s)) {
				tmp.addPlace(s);
				places.add(s);
			}
		}
		regions.put(regionName, tmp);
		return tmp.getPlaceNames();
	}
	
	public List<String> addCarrier(String carrierName, String... regionNames) { 
		Carrier tmp = new Carrier(carrierName);
		for(String r:regionNames) {
			if(regions.containsKey(r)) {
				tmp.addRegion(regions.get(r));
			}
		}
		carriers.put(carrierName, tmp);
		return tmp.getRegion();
	}
	
	public List<String> getCarriersForRegion(String regionName) { 
		return carriers.values().stream().
				filter(x -> x.getRegion().contains(regionName)).
				map(Carrier::getName).
				collect(Collectors.toList());
	}
	
//R2
	public void addRequest(String requestId, String placeName, String productId) 
			throws TMException {
		if(requests.containsKey(requestId))
			throw new TMException();
		if(!places.contains(placeName))
			throw new TMException();
		requests.put(requestId, new Request(this, requestId, placeName, productId));
	}
	
	public void addOffer(String offerId, String placeName, String productId) 
			throws TMException {
		if(offers.containsKey(offerId))
			throw new TMException();
		if(!places.contains(placeName))
			throw new TMException();
		offers.put(offerId, new Offer(offerId, placeName, productId));
	}
	

//R3
	public void addTransaction(String transactionId, String carrierName, String requestId, String offerId) 
			throws TMException {
		Request r = requests.get(requestId);
		Offer o = offers.get(offerId);
		for(Transaction t:transactions.values()) {
			if(t.getRequest().getId().equals(requestId) || t.getOffer().getId().equals(offerId)) {
				throw new TMException();
			}
		}
		if(!o.getProductId().equals(r.getProductId()))
			throw new TMException();
		Carrier c = carriers.get(carrierName);
		List<String> servedPlaces = c.getRegions().stream().flatMap(x -> x.getPlaceNames().stream()).collect(Collectors.toList());
		
		if(!servedPlaces.contains(o.getPlaceName()) || !servedPlaces.contains(r.getPlaceName()))
			throw new TMException();
		
		transactions.put(transactionId, new Transaction(transactionId,c, r, o));
	}
	
	public boolean evaluateTransaction(String transactionId, int score) {
		if(score < 1 || score > 10)
			return false;
		transactions.get(transactionId).setScore(score);
		return true;
	}
	
//R4
	public SortedMap<Long, List<String>> deliveryRegionsPerNT() {
		return transactions.values().stream().collect(
				Collectors.groupingBy(
						(Transaction t) -> t.getRequest().getRegionName(),
						TreeMap::new,
						Collectors.counting()
						)
				).entrySet().stream().collect(Collectors.groupingBy(
						Map.Entry::getValue,
						() -> new TreeMap<Long, List<String>>(reverseOrder()),
						Collectors.mapping(e -> e.getKey(), Collectors.toList())
						)
				);
	}
	
	public SortedMap<String, Integer> scorePerCarrier(int minimumScore) {
		return transactions.values().stream().filter(t -> t.getScore() >= minimumScore).
				collect(Collectors.groupingBy(
						t -> t.getCarrier().getName(),
						TreeMap::new,
						Collectors.summingInt(Transaction::getScore)
						)
				);
	}
	
	public SortedMap<String, Long> nTPerProduct() {
		return transactions.values().stream().collect(
				Collectors.groupingBy(
						t ->t.getOffer().getProductId(),
						TreeMap::new,
						Collectors.counting()
						)
				);
	}

	public Collection<Region> getRegions() {
		return regions.values();
	}
	
	
}

