package transactions;

public class Transaction {
	private String transactionId;
	private Carrier carrier;
	private Request request;
	private Offer offer;
	private int score;

	public Transaction(String transactionId, Carrier carrier, Request request, Offer offer) {
		this.transactionId = transactionId;
		this.carrier = carrier;
		this.request = request;
		this.offer = offer;
		score = 0;
	}

	public Request getRequest() {
		return request;
	}

	public Offer getOffer() {
		return offer;
	}
	
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Carrier getCarrier() {
		// TODO Auto-generated method stub
		return carrier;
	}

}
