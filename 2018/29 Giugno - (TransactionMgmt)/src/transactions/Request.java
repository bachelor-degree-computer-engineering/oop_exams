package transactions;

public class Request {
	private String id;
	private String placeName;
	private String productId;
	private TransactionManager tm;
	
	public Request(TransactionManager tm,String id, String placeName, String productId) {
		this.id = id;
		this.placeName = placeName;
		this.productId = productId;
		this.tm = tm;
	}


	public String getId() {
		return id;
	}


	public String getPlaceName() {
		return placeName;
	}


	public String getProductId() {
		return productId;
	}
	
	public String getRegionName() {
		for(Region r:tm.getRegions()) {
			if(r.getPlaceNames().contains(placeName))
				return r.getName();
		}
		return null;
	}
}
