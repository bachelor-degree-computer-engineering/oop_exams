package it.polito.oop.milliways;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Hall {
	public int id;
	List<String> facilities = new LinkedList<>();
	List<Party> seats = new LinkedList<>();
	
	public Hall(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void addFacility(String facility) throws MilliwaysException {
		if(facilities.contains(facility))
			throw new MilliwaysException();
		else {
			facilities.add(facility);
		}
	}

	public List<String> getFacilities() {
        Collections.sort(facilities);
        return facilities;
	}
	
	public int getNumFacilities(){
        return facilities.size();
	}

	public boolean isSuitable(Party party) {
		return facilities.containsAll(party.getRequirements());
	}
	
	public boolean equals(Hall tmp) {
		return id == tmp.getId();
	}
	
	public void seat(Party party) {
		seats.add(party);
	}
}
