package it.polito.oop.milliways;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Restaurant {
	
	private Map<String, Race> races = new HashMap<>();
	private List<Hall> halls = new LinkedList<>();
	private List<Party> parties = new LinkedList<>();
	
    public Restaurant() {
	}
	
	public Race defineRace(String name) throws MilliwaysException{
	    if(races.containsKey(name))
	    	throw new MilliwaysException();
	    else {
	    	Race tmp = new Race(name);
	    	races.put(name, tmp);
	    	return tmp;
	    }
	}
	
	public Party createParty() {
	    Party tmp = new Party();
	    parties.add(tmp);
	    return tmp;
	}
	
	public Hall defineHall(int id) throws MilliwaysException{
	    Hall tmp = new Hall(id);
		for(Hall h:halls) {
			if(h.equals(tmp))
				throw new MilliwaysException();
		}
		halls.add(tmp);
		return tmp;
	}

	public List<Hall> getHallList() {
		return halls;
	}
	

	public Hall seat(Party party, Hall hall) throws MilliwaysException {
        if(!hall.isSuitable(party))
        	throw new MilliwaysException();
        else {
        	hall.seat(party);
        	return hall;
        }
	}

	public Hall seat(Party party) throws MilliwaysException {
        for(Hall t:halls) {
        	if(t.isSuitable(party)) {
        		t.seat(party);
            	return t;
        	}
        }
        throw new MilliwaysException();
	}

	public Map<Race, Integer> statComposition() {
        return parties.stream().
        		flatMap(e -> e.getComposition().entrySet().stream())
        		.collect(Collectors.groupingBy(
        				Map.Entry::getKey,
        				Collectors.summingInt(Map.Entry::getValue)
        				)
        		);
	}

	public List<String> statFacility() {
		Comparator<Map.Entry<String,Long>> c = 
				comparing(Map.Entry::getValue, 
						  reverseOrder());
		c = c.thenComparing(Map.Entry::getKey);
		return halls.stream().flatMap(h -> h.getFacilities().stream()).
				collect(Collectors.groupingBy(
						x->x,
						Collectors.counting()
						)
				).entrySet().stream().sorted(c).
				map(Map.Entry::getKey)
				.collect(Collectors.toList());
	}
	
	public Map<Integer,List<Integer>> statHalls() {
        return halls.stream().
        		collect(Collectors.groupingBy(
        				Hall::getNumFacilities, 
        				TreeMap::new, 
        				Collectors.mapping(Hall::getId,
        						Collectors.collectingAndThen(Collectors.toList(),
        														l->{
        															Collections.sort(l);
        															return l;
        														}
        						)
        				)
        		));
	}

}
