package it.polito.oop.milliways;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Party {
	public Map<Race, Integer> tavola = new HashMap<>();
	
	
    public void addCompanions(Race race, int num) {
    	if(tavola.containsKey(race)) {
    		int oldValue = tavola.get(race);
    		tavola.put(race, oldValue + num);
    	}
    	else
    		tavola.put(race, num);
	}

	public int getNum() {
        return tavola.values().stream().collect(Collectors.summingInt(x->x));
	}

	public int getNum(Race race) {
        return tavola.get(race);
	}
	public Map<Race, Integer> getComposition(){
		return tavola;
	}
	
	public List<String> getRequirements() {
        return tavola.keySet().stream().
        		flatMap(x -> x.getRequirements().stream()).
        		distinct().
        		sorted().collect(Collectors.toList());
	}

    public Map<String,Integer> getDescription(){
        Map <String, Integer> tmp = new HashMap<>();
        tavola.keySet().stream().
        	forEach(x -> tmp.put(x.getName(), tavola.get(x)));
        return tmp;
    }

}
