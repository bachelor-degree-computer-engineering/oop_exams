package it.polito.oop.elective;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Course {
	private String name;
	private int avaiblePosition;
	private Long[] numberOfRequest = new Long[3];
	private List<Student> enrolledIn =  new LinkedList<>();
	
	public Course(String name, int avaiblePosition) {
		this.name = name;
		this.avaiblePosition = avaiblePosition;
		this.numberOfRequest = new Long[3];
		for(int i=0; i<3; i++)
			numberOfRequest[i] = new Long(0);
	}

	public String getName() {
		return name;
	}

	public int getAvaiblePosition() {
		return avaiblePosition;
	}
	
	public void setRequest(int pos) {
		numberOfRequest[pos]++;
	}
	
	public List<Long> getRequest(){
		return Arrays.asList(numberOfRequest);
	}

	public void enroll(Student s) {
		enrolledIn.add(s);
		avaiblePosition--;
	}
	
	public List<String> getStudents(){
		return enrolledIn.stream().sorted(Comparator.comparing(Student::getAvg, Comparator.reverseOrder())).map(Student::getId).collect(Collectors.toList());
	}
}
