package it.polito.oop.elective;

import java.util.ArrayList;
import java.util.List;

public class Student {
	private String id;
	private double avg;
	List<String> request = new ArrayList<>();
	private boolean enrolled = false;
	private Course enroll;
	private int enrollIn;
	
	
	public Student(String id, double avg) {
		this.id = id;
		this.avg = avg;
	}
	public String getId() {
		return id;
	}
	public double getAvg() {
		return avg;
	}
	public void setRequest(List<String> courses) {
		this.request = courses;
	}
	
	public boolean enrolledIn() {
		return enrolled;
	}
	
	public List<String> getRequest() {
		return request;
	}
	
	public void enroll(Course course, int pos) {
		enrolled = true;
		enroll = course;
		enrollIn = pos;
	}
	
	public int choice() {
		return enrollIn;
	}
}
