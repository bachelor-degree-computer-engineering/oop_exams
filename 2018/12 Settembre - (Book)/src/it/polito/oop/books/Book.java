package it.polito.oop.books;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Book {
	private Map <String , Topic> topics =  new HashMap<>();
	private List<Question> questions = new LinkedList<>();
	private List<TheoryChapter> theoryChapters = new LinkedList<>();
	private List<ExerciseChapter> exerciseChapters = new LinkedList<>();

    /**
	 * Creates a new topic, if it does not exist yet, or returns a reference to the
	 * corresponding topic.
	 * 
	 * @param keyword the unique keyword of the topic
	 * @return the {@link Topic} associated to the keyword
	 * @throws BookException
	 */
	public Topic getTopic(String keyword) throws BookException {
		if(keyword==null || keyword.isEmpty())
			throw new BookException();
	    if(topics.containsKey(keyword))
	    	return topics.get(keyword);
	    Topic tmp = new Topic(keyword);
	    topics.put(keyword, tmp);
	    return tmp;
	}

	public Question createQuestion(String question, Topic mainTopic) {
        Question tmp = new Question(question, mainTopic);
        questions.add(tmp);
        return tmp;
	}

	public TheoryChapter createTheoryChapter(String title, int numPages, String text) {
        TheoryChapter tmp = new TheoryChapter(title, text, numPages);
        theoryChapters.add(tmp);
		return tmp;
	}

	public ExerciseChapter createExerciseChapter(String title, int numPages) {
		ExerciseChapter tmp = new ExerciseChapter(title, numPages);
		exerciseChapters.add(tmp);
		return tmp;
	}

	public List<Topic> getAllTopics() {
        List<Topic> tmp = theoryChapters.stream().flatMap((TheoryChapter c) -> c.getTopics().stream()).
        		collect(Collectors.toList());
        
        tmp.addAll(exerciseChapters.stream().flatMap((ExerciseChapter c) -> c.getTopics().stream()).
                		collect(Collectors.toList())
        		);
        return tmp.stream().distinct().sorted(Comparator.comparing(Topic::getKeyword)).
        		collect(Collectors.toList());
        }

	public boolean checkTopics() {
		List<Topic> ex = exerciseChapters.stream().flatMap((ExerciseChapter c) -> c.getTopics().stream()).
        		collect(Collectors.toList());
		boolean flag = true;
		for(Topic t: ex) {
			if(!theoryChapters.stream().flatMap((TheoryChapter c) -> c.getTopics().stream()).
					collect(Collectors.toList()).contains(t))
				flag = false;
		}
		return flag;
	}

	public Assignment newAssignment(String ID, ExerciseChapter chapter) {
        return new Assignment(ID, chapter);
	}
	
    /**
     * builds a map having as key the number of answers and 
     * as values the list of questions having that number of answers.
     * @return
     */
    public Map<Long,List<Question>> questionOptions(){
        return questions.stream().collect(
        		Collectors.groupingBy(
        				Question::numAnswers
        				));
    }
}
