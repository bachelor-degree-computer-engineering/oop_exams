package it.polito.oop.books;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class TheoryChapter {
	private String title, text;
	private int numPages;
	private List<Topic> topics = new LinkedList<>();
	
    public TheoryChapter(String title, String text, int numPages) {
		this.title = title;
		this.text = text;
		this.numPages = numPages;
	}

	public String getText() {
		return text;
	}

    public void setText(String newText) {
    	this.text = newText;
    }


	public List<Topic> getTopics() {
        Collections.sort(topics, Comparator.comparing(Topic::getKeyword));
        return topics;
	}

    public String getTitle() {
        return title;
    }

    public void setTitle(String newTitle) {
    	this.title = newTitle;
    }

    public int getNumPages() {
        return numPages;
    }
    
    public void setNumPages(int newPages) {
    	this.numPages = newPages;
    }
    
    public void addTopic(Topic topic) {
    	if(!topics.contains(topic)) {
    		topics.add(topic);
	    	List<Topic> tmp = topic.getSubTopics();
	    	for(Topic t:tmp)
	    		this.addTopic(t);
    	}
    }
    
}
