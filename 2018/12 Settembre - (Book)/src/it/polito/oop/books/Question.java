package it.polito.oop.books;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Question {
	private class Answer{
		private String answerText;
		private boolean correct;
		
		public Answer(String answerText, boolean correct) {
			this.answerText = answerText;
			this.correct = correct;
		}
		public String getText() {
			return answerText;
		}
		public boolean isCorrect() {
			return correct;
		}
	}
	private String text;
	private Topic mainTopic;
	private List<Answer> answers = new LinkedList<Answer>();
	
	public Question(String text, Topic mainTopic) {
		this.text = text;
		this.mainTopic = mainTopic;
	}

	public String getQuestion() {
		return text;
	}
	
	public Topic getMainTopic() {
		return mainTopic;
	}

	public void addAnswer(String answer, boolean correct) {
		answers.add(new Answer(answer, correct));
	}
	
    @Override
    public String toString() {
        return text + "(" + mainTopic.getKeyword() + ")";
    }

	public long numAnswers() {
	    return answers.stream().count();
	}

	public Set<String> getCorrectAnswers() {
		return answers.stream().filter(Answer::isCorrect).
				map(Answer::getText).collect(Collectors.toSet());
	}

	public Set<String> getIncorrectAnswers() {
        return answers.stream().filter(a -> !(a.isCorrect())).
				map(Answer::getText).collect(Collectors.toSet());
	}
}
