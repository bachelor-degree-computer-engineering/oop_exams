package it.polito.oop.books;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import static java.util.Comparator.*;

public class Topic {
	
	private String keyword;
	private List< Topic> subTopics = new LinkedList<>();
	public Topic(String keyword){
		this.keyword = keyword;
	}
	
	public String getKeyword() {
        return keyword;
	}
	
	@Override
	public String toString() {
	    return keyword;
	}

	public boolean addSubTopic(Topic topic) {
        if(subTopics.contains(topic))
        	return false;
        subTopics.add(topic);
        return true;
	}

	/*
	 * Returns a sorted list of subtopics. Topics in the list *MAY* be modified without
	 * affecting any of the Book topic.
	 */
	public List<Topic> getSubTopics() {
        Collections.sort(subTopics, comparing(Topic::getKeyword));
        return subTopics;
	}
}
