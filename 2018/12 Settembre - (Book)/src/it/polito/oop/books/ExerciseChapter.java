package it.polito.oop.books;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class ExerciseChapter {
	private String title;
	int numPages;
	private List<Question> questions = new LinkedList<>();
	
	
    public ExerciseChapter(String title, int numPages) {
		this.title = title;
		this.numPages = numPages;
	}


	public List<Topic> getTopics() {
        return questions.stream().map(Question::getMainTopic).
        		distinct().
        		sorted(Comparator.comparing(Topic::getKeyword)).
        		collect(Collectors.toList());
	};
	

    public String getTitle() {
        return title;
    }

    public void setTitle(String newTitle) {
    	this.title = newTitle;
    }

    public int getNumPages() {
        return numPages;
    }
    
    public void setNumPages(int newPages) {
    	this.numPages = newPages;
    }
    

	public void addQuestion(Question question) {
		questions.add(question);
	}


	public Question question(Question q) {
		for(Question question:questions) {
			if(question.equals(q))
				return question;
		}
		return null;
	}	
}
