package it.polito.oop.futsal;

public class Booking {
	private Associate associate;
	private Time bookTime;
	/**
	 * @param associate
	 * @param bookTime
	 */
	public Booking(Associate associate, Time bookTime) {
		this.associate = associate;
		this.bookTime = bookTime;
	}
	/**
	 * @return the associate
	 */
	public Associate getAssociate() {
		return associate;
	}
	/**
	 * @return the bookTime
	 */
	public Time getBookTime() {
		return bookTime;
	}
	
	
}
