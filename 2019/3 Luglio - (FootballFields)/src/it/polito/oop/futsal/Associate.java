package it.polito.oop.futsal;

public class Associate {
	private int id;
	private String first,  last, mobile;
	boolean hasBooked = false;
	
	/**
	 * @param id
	 * @param first
	 * @param last
	 * @param mobile
	 */
	public Associate(int id, String first, String last, String mobile) {
		this.id = id;
		this.first = first;
		this.last = last;
		this.mobile = mobile;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the first
	 */
	public String getFirst() {
		return first;
	}
	/**
	 * @return the last
	 */
	public String getLast() {
		return last;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	
	/**
	 * @return the hasBooked
	 */
	public boolean hasBooked() {
		return hasBooked;
	}
}
