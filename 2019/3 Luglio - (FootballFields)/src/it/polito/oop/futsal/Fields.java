package it.polito.oop.futsal;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;

/**
 * Represents a infrastructure with a set of playgrounds, it allows teams
 * to book, use, and  leave fields.
 *
 */
public class Fields { 
    public static class Features {
        public final boolean indoor; // otherwise outdoor
        public final boolean heating;
        public final boolean ac;
        public Features(boolean i, boolean h, boolean a) {
            this.indoor=i; this.heating=h; this.ac = a;
        }
    }
    
    private int currentField = 1;
    private Map<Integer, Field> fields = new TreeMap<>();
    
    private int currentAssociate = 1;
    private Map<Integer, Associate> associates = new TreeMap<>();

    private Time openingTime, closingTime;
    
    public void defineFields(Features... features) throws FutsalException {
        for(Features f:features) {
        	if(f.indoor==false && (f.ac || f.heating))
        		throw new FutsalException();
        	fields.put(currentField, new Field(currentField, f));
        	currentField++;
        }
    }
    
    public long countFields() {
        return currentField-1;
    }

    public long countIndoor() {
        return fields.values().stream().filter(f -> f.getFeatures().indoor).count();
    }
    
    public String getOpeningTime() {
        return openingTime.toString();
    }
    
    public void setOpeningTime(String time) {
    	openingTime = new Time(time);
    }
    
    public String getClosingTime() {
        return closingTime.toString();
    }
    
    public void setClosingTime(String time) {
    	closingTime = new Time(time);
    }

    public int newAssociate(String first, String last, String mobile) {
    	associates.put(currentAssociate, new Associate(currentAssociate, first, last, mobile));
        return currentAssociate++;
    }
    
    public String getFirst(int associate) throws FutsalException {
       if(!associates.containsKey(associate))
    	   throw new FutsalException();
       return associates.get(associate).getFirst();
    }
    
    public String getLast(int associate) throws FutsalException {
    	if(!associates.containsKey(associate))
     	   throw new FutsalException();
        return associates.get(associate).getLast();
    }
    
    public String getPhone(int associate) throws FutsalException {
    	if(!associates.containsKey(associate))
     	   throw new FutsalException();
        return associates.get(associate).getMobile();
    }
    
    public int countAssociates() {
        return currentAssociate - 1;
    }
    
    public void bookField(int field, int associate, String time) throws FutsalException {
    	if(!fields.containsKey(field) || !associates.containsKey(associate)
    			|| !openingTime.isAligned(new Time(time)))
    		throw new FutsalException();
    	associates.get(associate).hasBooked = true;
    	fields.get(field).book(associates.get(associate), new Time(time));
    }

    public boolean isBooked(int field, String time) throws FutsalException {
       if(!fields.containsKey(field))
    	   throw new FutsalException();
       return fields.get(field).isBooked(new Time(time));
    }
    

    public int getOccupation(int field) {
        return fields.get(field).getOccupation();
    }
    
    
    public List<FieldOption> findOptions(String time, Features required){
        return fields.values().stream()
        		.filter(f -> f.respect(required))
        		.filter(f -> !f.isBooked(new Time(time)))
        		.sorted(comparing(Field::getOccupation)
        				.thenComparing(Field::getId))
        		.collect(toList());
        		
    }
    
    public long countServedAssociates() {
        return associates.values().stream().filter(Associate::hasBooked).count();
    }
    
    public Map<Integer,Long> fieldTurnover() {
        return fields.values().stream()
        		.collect(toMap(
        				Field::getId,
        				Field::getNBooking
        			)
        		);
    }
    
    public double occupation() {
        return (double) fields.values().stream()
        		        .mapToLong(Field::getNBooking).sum() / (fields.size() * this.getSlots());
    }

	private int getSlots() {
		return closingTime.getH() - openingTime.getH();
	}
    
}
