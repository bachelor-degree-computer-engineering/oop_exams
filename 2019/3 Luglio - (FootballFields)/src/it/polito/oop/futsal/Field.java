package it.polito.oop.futsal;

import java.util.HashMap;
import java.util.Map;

import it.polito.oop.futsal.Fields.Features;

public class Field  implements FieldOption{
	private int id;
	private Features features;
	private Map<String, Booking> bookings = new HashMap<>();
	long nbooking = 0;
	/**
	 * @param id
	 * @param features
	 */
	public Field(int id, Features features) {
		this.id = id;
		this.features = features;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the features
	 */
	public Features getFeatures() {
		return features;
	}
	
	public void book(Associate a, Time t) {
		bookings.put(t.toString(), new Booking(a, t));
		nbooking++;
	}
	
	public boolean isBooked(Time t) {
		//System.out.println("Il keySet contiene: " + bookings.keySet());
		return bookings.containsKey(t.toString());
	}
	public int getOccupation() {
		return bookings.size();
	}
	
	public boolean respect(Features required) {
		boolean value = false;
		if(required.indoor == true) {
			value = required.indoor == features.indoor;
			if(!value) return false;
		}
		if(required.ac == true) {
			value = required.ac == features.ac;
			if(!value) return false;
		}
		if(required.heating == true) {
			value = required.heating == features.heating;
			if(!value) return false;
		}
		return true;
	}
	
	public Long getNBooking() {
		return nbooking;
	}
	
	@Override
	public int getField() {
		return id;
	}
}
