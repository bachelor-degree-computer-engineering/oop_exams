package it.polito.oop.futsal;

public class Time {
	private int h, m;
	
	public Time(String time) {
		String[] s = time.split(":");
		this.h = Integer.parseInt(s[0]);
		this.m = Integer.parseInt(s[1]);
	}

	/**
	 * @return the h
	 */
	public int getH() {
		return h;
	}

	/**
	 * @return the m
	 */
	public int getM() {
		return m;
	}
	public boolean isAligned(Time t) {
		return (t.getM() == m);
	}
	
	public String toString() {
		return String.format("%2d:%2d", h, m);
	}
	
	public boolean equals(Time o) {
		return (o.getH() == this.getH()) && (o.getM() == this.getM());
	}
}
