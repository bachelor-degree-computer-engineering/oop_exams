package BankServices;

import java.util.LinkedList;

import java.util.List;
import java.util.stream.Collectors;
import static java.util.Comparator.*;
import static java.util.stream.Collectors.*;

public class Account {
	private int code;
	private String name;
	private int date;
	private double saldo;
	private int lastOperation;
	private boolean closed = false;
	
	private List<Operation> movements = new LinkedList<>();
	
	public Account(int code, String name, int date, double initial) {
		this.code = code;
		this.name = name;
		this.date = date;
		lastOperation = date;
		this.saldo = 0;
		this.deposit(date, initial);
	}

	public String toString() {
		return code + "," + name + "," + lastOperation + "," + saldo;
	}
	
	public void deposit(int date, double sum) {
		Deposit d = new Deposit(date, sum);
		movements.add(d);
		
		saldo += sum;
		if(date > lastOperation)
			lastOperation = date;
	}
		
	public void withdraw(int date, double sum) {
		Withdrawal w = new Withdrawal(date, sum);
		movements.add(w);
		saldo -= sum;
		if(date > lastOperation)
			lastOperation = date;
	}
	
	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public int getDate() {
		return date;
	}

	public double getSaldo() {
		return saldo;
	}

	public int getLastOperation() {
		return lastOperation;
	}

	public List<Operation> getMovements() {
		return movements.stream().
				sorted(comparing(Operation::getDate, reverseOrder())).
				collect(toList());
	}
	
	public List<Deposit> getDeposits() {
		return movements.stream().
					filter(Operation::isDeposit).
					map(Operation::getDeposit).
					collect(toList());
	}

	public List<Withdrawal> getWithdrawals() {
		return movements.stream().
				filter(o -> !o.isDeposit()).
				sorted(comparing(Operation::getValue, reverseOrder())).
				map(Operation::getWithdrawal).
				collect(toList());
	}

	public void deleteAccount(int date) {
		this.withdraw(date, saldo);
		this.closed = true;
		if(date > lastOperation)
			lastOperation = date;
	}
	
}
