package BankServices;

public abstract class Operation {
	
	public abstract String toString();
	public abstract int getDate();
	public abstract double getValue();
	public abstract boolean isDeposit();
	public abstract Deposit getDeposit();
	public abstract Withdrawal getWithdrawal();
}
