package BankServices;

public class Withdrawal extends Operation {
	private int date;
	private double value;	
	
	public boolean isDeposit() {
		return false;
	}

	public Withdrawal(int date, double value) {
		this.date = date;
		this.value = value;
	}
	
	
	public int getDate() {
		return date;
	}


	public double getValue() {
		return value;
	}


	@Override
	public String toString() {
		return date + "," + value + "-";
	}

	@Override
	public Deposit getDeposit() {
		return null;
	}

	@Override
	public Withdrawal getWithdrawal() {
		return this;
	}
}
