package BankServices;

public class Deposit extends Operation{
	private int date;
	private double value;
		
	public Deposit(int date, double value) {
		this.date = date;
		this.value = value;
	}

	public int getDate() {
		return date;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return date + "," + value + "+";
	}	
	
	public boolean isDeposit() {
		return true;
	}

	@Override
	public Deposit getDeposit() {
		return this;
	}

	@Override
	public Withdrawal getWithdrawal() {
		return null;
	}
}
