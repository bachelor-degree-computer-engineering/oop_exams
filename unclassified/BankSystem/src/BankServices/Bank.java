package BankServices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;
public class Bank {
	private String name;
	private int currentCode = 1;
	private Map<Integer, Account> accounts = new HashMap<>();
	
	public Bank(String n) {
		this.name = n;
	}
	
	public String getName() {
		return name;
	}
	
	public int createAccount(String name, int date, double initial) {
		Account a = new Account(currentCode, name, date, initial);
		accounts.put(currentCode, a);
		return currentCode++;
	}
	
	public Account deleteAccount(int code, int date) throws InvalidCode {
		if(!accounts.containsKey(code))
			throw new InvalidCode();
		Account a = accounts.get(code);
		a.deleteAccount(date);
		accounts.remove(code);
		return a;
	}
	
	public Account getAccount(int code) throws InvalidCode {
		Account a = accounts.get(code);
		if(a == null) throw new InvalidCode();
		return a;
	}

	public void deposit(int code, int date, double value) throws InvalidCode {
		if(!accounts.containsKey(code))
			throw new InvalidCode();
		Account a = accounts.get(code);
		a.deposit(date, value);
	}

	public void withdraw(int code, int date, double value) throws InvalidCode, InvalidValue {
		if(!accounts.containsKey(code))
			throw new InvalidCode();
		Account a = accounts.get(code);
		if(a.getSaldo() < value)
			throw new InvalidValue();
		a.withdraw(date, value);
	}
	
	public void transfer(int fromCode, int toCode, int date, double value) throws InvalidCode, InvalidValue {
		this.withdraw(fromCode, date, value);
		this.deposit(toCode, date, value);
	}
	
	public double getTotalDeposit() {
		return accounts.values().stream().mapToDouble(Account::getSaldo).sum();
	}
	
	public List<Account> getAccounts() {
		return accounts.values().stream()
				.sorted(comparing(Account::getCode))
				.collect(toList());
	}
	
	public List<Account> getAccountsByBalance(double low, double high) {
		return accounts.values().stream()
				.filter(a -> a.getSaldo() >= low && a.getSaldo() <= high)
				.sorted(comparing(Account::getSaldo, reverseOrder()))
				.collect(toList());
	}
	
	public long getPerCentHigher(double min) {
		long n = accounts.values().stream()
				.filter(a -> a.getSaldo() >= min).count();
		long d = accounts.values().size();
		 
		long v = (n*100)/d;
		return v;
	}
}
