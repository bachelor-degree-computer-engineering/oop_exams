package evaluation;
import java.util.*;

import static java.util.stream.Collectors.*;
import static java.util.Comparator.*;

/**
 * Facade class for the research evaluation system
 *
 */
public class Evaluations {
	private Map<Integer, Integer> levels = new TreeMap<>();
	private Map<String, Journal> journals = new TreeMap<>();
	private Set<String> disciplines = new TreeSet<>();
	private Map<String, Group> groups = new TreeMap<>();

    //R1
    /**
     * Define number of levels and relative points.
     * 
     * Levels are numbered from 1 on, and points must be strictly decreasing
     *  
     * @param points points for the levels
     * @throws EvaluationsException thrown if points are not decreasing
     */
    public void addPointsForLevels (int... points) throws EvaluationsException {
    	for(int i=1; i < points.length; i++)
    		if(points[i] > points[i-1])
    			throw new EvaluationsException();
    	for(int i=0; i<points.length; i++) {
    			levels.put(i+1, points[i]);
    	}
    }

    /**
     * Retrieves the points for the given level.
     * 
     * @param level level for which points are required
     * @return points for the level
     */
    public int getPointsOfLevel (int level) {
        return levels.get(level);
    }

    /**
     * Add a new journal for a given disciplines and provides the corresponding level.
     * 
     * The level determines the points for the article published in the journal.
     * 
     * @param name name of the new journal
     * @param discipline reference discipline for the journal
     * @param level level for the journal.
     * @throws EvaluationsException thrown if the specified level does not exist
     */
    public void addJournal (String name, String discipline, int level) throws EvaluationsException {
    	if(!levels.containsKey(level))
    		throw new EvaluationsException();
    	Journal j = new Journal(name, discipline, level, levels.get(level));
    	journals.put(name, j);
    	disciplines.add(discipline);
    }

    /**
     * Retrieves number of journals.
     * 
     * @return journals count
     */
    public int countJournals() {
        return journals.size();
    }

    /**
     * Retrieves all the journals for a given discipline.
     * 
     * @param discipline the required discipline
     * @return list of journals (sorted alphabetically)
     */
    public List<String> getJournalNamesOfAGivenDiscipline(String discipline) {
        List<String> tmp = journals.values().stream()
        		.filter(j -> j.getDiscipline().equals(discipline))
        		.map(Journal::getName)
        		.sorted()
        		.collect(toList());
        return tmp;
    }

    //R2
    /**
     * Add a research group and the relative disciplines.
     * 
     * @param name name of the research group
     * @param disciplines list of disciplines
     * @throws EvaluationsException thrown in case of duplicate name
     */
    public void addGroup (String name, String... disciplines) throws EvaluationsException {
    	if(groups.containsKey(name))
    		throw new EvaluationsException();
    	Group g = new Group(this, name, disciplines);
    	groups.put(name, g);
    }

    /**
     * Define the members for a previously defined research group.
     * 
     * @param groupName name of the group
     * @param memberNames list of group members
     * @throws EvaluationsException thrown if name not previously defined.
     */
    public void setMembers (String groupName, String... memberNames) throws EvaluationsException {
    	if(!groups.containsKey(groupName))
    		throw new EvaluationsException();
    	Group g = groups.get(groupName);
    	for(String member:memberNames)
    		g.addMember(member);
    }

    /**
     * Return list of members of a group.
     * The list is sorted alphabetically.
     * 
     * @param groupName name of the group
     * @return list of members
     */
    public List<String >getMembers(String groupName){
        if(!groups.containsKey(groupName))
        	return new LinkedList<String>();
        return groups.get(groupName).getMembers().stream()
        		.sorted()
        		.collect(toList());
    }
    

    /**
     * Retrieves the group names working on a given discipline
     * 
     * @param discipline the discipline of interest
     * @return list of group names sorted alphabetically
     */
    public List<String> getGroupNamesOfAGivenDiscipline(String discipline) {
        if(!disciplines.contains(discipline))
        	return new LinkedList<String>();
        return groups.values().stream()
        		.filter(g -> g.getDisciplines().contains(discipline))
        		.map(Group::getName)
        		.sorted()
        		.collect(toList());
    }

    //R3
    /**
     * Add a new journal articles, with a given title and the list of authors.
     * 
     * The journal must have been previously defined.
     * 
     * The authors (at least one) are members of research groups.
     * 
     * @param title title of the article
     * @param journalName name of the journal
     * @param authorNames list of authors
     * @throws EvaluationsException thrown if journal not defined or no author provided
     */
    public void addPaper (String title, String journalName, String... authorNames) throws EvaluationsException {
    	if(!journals.containsKey(journalName) || authorNames.length < 1)
    		throw new EvaluationsException();
    	Journal j = journals.get(journalName);
    	Paper p = new Paper(title, j, authorNames);
    	j.papers.add(p);
    }



    /**
     * Retrieves the titles of the articles authored by a member of a research group
     * 
     * @param memberName name of the group member
     * @return list of titles sorted alphabetically
     */
    public List<String> getTitlesOfAGivenAuthor (String memberName) {
        return journals.values().stream()
        		.flatMap(j->j.papers.stream())
        		.filter(p ->p.getAuthor().contains(memberName))
        		.map(Paper::getTitle)
        		.sorted()
        		.collect(toList());
    }


    //R4
    /**
     * Returns the points for a given group member.
     * 
     * Points are collected for each article the member authored.
     * The points are those corresponding to the level of the
     * journal where the article is published, divided by
     * the total number of authors.
     * 
     * The total points are eventually rounded to the closest integer.
     * 
     * @param memberName name of the group member
     * @return total points
     */
    public int getPointsOfAGivenAuthor (String memberName) {
    	double value = journals.values().stream()
    					.flatMap(j -> j.papers.stream())
    					.filter(p -> p.getAuthor().contains(memberName))
    					.mapToDouble(Paper::getPoints).sum();
        return (int) Math.round(value);
    }

    /**
     * Computes the total points collected by all members of all groups
     *  
     * @return the total points
     */
    public int evaluate() {
    	double value = journals.values().stream()
    			.flatMap(j -> j.papers.stream())
    			.mapToDouble(Paper::getPointswithoutAuthor).sum();
        return (int) Math.round(value);
    }


    //R5 Statistiche
    /**
     * For each group return the total points collected
     * by all the members of each research group.
     * 
     * Group names are sorted alphabetically.
     * 
     * @return the map associating group name to points
     */
    public SortedMap<String, Integer> pointsForGroup() {
        return groups.values().stream()
        		.collect(toMap(
        				Group::getName,
        				Group::getPoints,
        				(g1,g2) -> g1,
        				TreeMap::new
        			)
        		);
    }

    /**
     * For each amount of points returns a list of
     * the authors (group members) that achieved such score.
     * 
     * Points are sorted in decreasing order.
     * 
     * @return the map linking the number of point to the list of authors
     */
    public SortedMap<Integer, List<String>> getAuthorNamesPerPoints () {
        List<String> authors = groups.values().stream()
        						.flatMap(g -> g.getMembers().stream())
        						.distinct()
        						.filter(a -> this.getPointsOfAGivenAuthor(a) > 0)
        						.collect(toList());
        SortedMap<String, Integer> map = new TreeMap<>();
        for(String a: authors) {
        	map.put(a, this.getPointsOfAGivenAuthor(a));
        }
        return map.entrySet().stream()
        		.collect(groupingBy(
        					Map.Entry::getValue,
        					() -> new TreeMap<Integer, List<String>>(reverseOrder()),
        					mapping(Map.Entry::getKey, toList())
        				)
        			);
    }


}