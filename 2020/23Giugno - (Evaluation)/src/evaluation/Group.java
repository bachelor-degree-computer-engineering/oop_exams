package evaluation;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Group {
	private String name;
	private Set<String> disciplines = new TreeSet<>();
	private List<String> members = new LinkedList<>();
	private Evaluations e;
	public Group(Evaluations e, String name, String... disciplines) {
		this.name = name;
		this.e = e;
		for(String s:disciplines)
			this.disciplines.add(s);
	}

	public String getName() {
		return name;
	}

	public Set<String> getDisciplines() {
		return disciplines;
	}

	public void addMember(String member) {
		members.add(member);
	}

	public List<String> getMembers() {
		return members;
	}
	
	public Integer getPoints() {
		int sum = 0;
		for(String m:members) {
			sum += e.getPointsOfAGivenAuthor(m);
		}
		return sum;
	}
}
