package evaluation;

import java.util.Set;
import java.util.TreeSet;

public class Paper {
	private String title;
	private Journal journal;
	private Set<String> author = new TreeSet<>();
	
	public Paper(String title, Journal journal, String... authors) {
		this.title = title;
		this.journal = journal;
		for(String s:authors)
			author.add(s);
	}

	public String getTitle() {
		return title;
	}

	public Journal getJournal() {
		return journal;
	}

	public Set<String> getAuthor() {
		return author;
	}
	
	public double getPoints() {
		return journal.getPoints()/author.size();
	}
	
	public double getPointswithoutAuthor() {
		return journal.getPoints();
	}
}
