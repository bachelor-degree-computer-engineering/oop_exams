package evaluation;

import java.util.LinkedList;
import java.util.List;

public class Journal {
	private String name;
	private String discipline;
	private int level;
	private int points = 0;
	
	public Journal(String name, String discipline, int level, int points) {
		this.name = name;
		this.discipline = discipline;
		this.level = level;
		this.points = points;
	}
	public String getName() {
		return name;
	}
	public String getDiscipline() {
		return discipline;
	}
	public int getLevel() {
		return level;
	}
	
	List<Paper> papers = new LinkedList<>();

	public List<Paper> getPapers() {
		return papers;
	}
	public int getPoints() {
		return points;
	}
	
	
	
	
}
